import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { pedido } from 'src/app/models/pedido';
import { Pedido_Item } from 'src/app/models/pedido_item';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit, OnChanges {
  //Observables de pedidos y Se esta cargando la pagina?
  @Input() pedidoView!: pedido;
  @Input() pedidosItemsView!: Observable<Pedido_Item[]>;
  @Input() articulosCargados!: boolean;

  pedido!: pedido;

  constructor() { }
  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
      this.pedido = this.pedidoView;

  }

}
