import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { pedido } from 'src/app/models/pedido';
import { Pedido_Item } from 'src/app/models/pedido_item';
import { PedidoService } from 'src/app/services/pedido/pedido.service';
import { PedidoItemService } from 'src/app/services/pedido_item/pedido-item.service';


@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css'],
})
export class PedidosComponent implements OnInit {
  //Observables de categorias y articulos
  pedidos!: Observable<pedido[]>;
  pedidosItems!: Observable<Pedido_Item[]>;

  //Filtros Value
  email!: any;
  fecha!: Date;
  fecha1!: Date;
  fecha2!: Date;

  //Se esta cargando la pagina?
  isLoaded = false;
  articulosCargados = false;

  pedido: pedido = {
    id_pedido: 0,
    fecha: new Date(),
    precioTotal: 0,
    iva: 0,
    usuario:{
      id_usuario: 0,
      username: '',
      password: '',
      email: '',
      nombre: '',
      apellidos: '',
    },
    franjaHoraria:{
      id_franjahoraria: 0,
      horaInicio: '',
      horaFinal: '',
      ocupacionMaxima: 0,
      ocupacionActual: 0
    }
  }
  constructor(private pedidoService: PedidoService, private pedidoItemService: PedidoItemService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.obtenerPedidos();
    this.obtenerPedidosService();
    this.changeDetectorRef.detectChanges();
  }

  mostrarPedido(pedido: pedido) {
    this.pedido = pedido;

  }

  //Obtener Pedidos de la BD
  obtenerPedidos() {
    this.pedidos = this.pedidoService.getPedidosList();
    this.pedidos.subscribe(data=> {
      this.pedido = data[0];
      this.isLoaded = true;

      setTimeout(() => {
        this.articulosCargados = true;
      }, 5000);

    });
  }

  obtenerPedidosService() {
    this.pedidosItems = this.pedidoItemService.getPedidosItemsList();
  }

  //Evento - Filtrar por Email
  onNameChangeFilter($event: any) {
    this.buscarPorEmail()
  }

  //Filtrar Articulos por Email
  buscarPorEmail() {
    this.pedidos = this.pedidos.pipe(map(pedidos => pedidos.filter(pedido => pedido.usuario.email.toLowerCase().includes( this.email.toLowerCase() )  ) ) )
  }

  //Evento - Filtrar por fecha
  onFechaChangeFilter($event: any) {
    if(!this.fecha){
      this.obtenerPedidos();
      if(this.email){
        this.buscarPorEmail();
      }
    }else{
      this.buscarPorFecha();
    }

  }

  //Filtrar Articulos por fecha
  buscarPorFecha() {
    this.pedidos = this.pedidos.pipe(map(pedidos => pedidos.filter(pedido => pedido.fecha == this.fecha ) ) )
  }

  //Evento - Filtrar por fecha Rango
  onFechaChangeRangoFilter($event: any) {
    if(!this.fecha1 || !this.fecha2){
      this.obtenerPedidos();
      if(this.email){
        this.buscarPorEmail();
      }
    }else{
      this.buscarPorFechaRango()
    }
  }

  //Filtrar Articulos por fecha Rango
  buscarPorFechaRango() {
    this.pedidos = this.pedidos.pipe(map(pedidos => pedidos.filter(pedido =>{ return pedido.fecha >= this.fecha1 &&  pedido.fecha <= this.fecha2 }) ) )
  }







}


