import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { usuario } from 'src/app/models/usuario';
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import { CarritoStorage } from 'src/app/services/carrito/carrito-storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private tokenStorage: TokenStorage,  private router: Router, private carritoStorage: CarritoStorage) { }

  user: usuario = {
    id_usuario: 0,
    username: '',
    password: '',
    email: '',
    nombre: '',
    apellidos: '',
  }

  isLoggedIn = false;


  contador = this.carritoStorage.getContador();

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.user = this.tokenStorage.getUser();
      this.isLoggedIn = true;
    }
    if(!this.isLoggedIn){
      this.router.navigate(['/login']);
    }
    this.isCarritoClean();
    this.carritoStorage.watchStorage().subscribe((data:string) => {
      this.contador = this.carritoStorage.getContador();
    })

  }

  isCarritoClean(){
    if(this.contador > 0){
      return true;
    }else{
      return false;
    }
  }

  logOut(){
    //force logout
    this.tokenStorage.logOut()
    this.reloadPage();
  }

  reloadPage() {
    window.location.reload();
  }

}
