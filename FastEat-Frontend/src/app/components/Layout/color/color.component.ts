import { Component, OnInit } from '@angular/core';

const COLOR_KEY = 'color-user';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.css']
})
export class ColorComponent implements OnInit {

  color!: string;

  constructor() { }

  ngOnInit(): void {
    if(!this.getCarrito()){
      this.saveCarritoLocalStorage('bg-theme2');
      this.color = this.getCarrito();
    }else{
      this.color = this.getCarrito();
    }

  }

  onChangueColor(color: string){
    this.saveCarritoLocalStorage(color);
  }

  public saveCarritoLocalStorage(Entrada: any) {
    window.localStorage.removeItem(COLOR_KEY);
    window.localStorage.setItem(COLOR_KEY, JSON.stringify(Entrada));
  }

  public getCarrito(): any {
    return JSON.parse(localStorage.getItem(COLOR_KEY)!);
  }

}
