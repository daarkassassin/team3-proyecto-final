import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs/operators';
import { usuario } from 'src/app/models/usuario';
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';
import { AlergenoService } from 'src/app/services/alergeno/alergeno.service';
import { Observable } from 'rxjs';
import { alergeno } from 'src/app/models/alergeno';
import { UsuarioAlergenoService } from 'src/app/services/usuario_alergeno/usuario-alergeno.service';
import { usuario_Alergeno } from 'src/app/models/usuario_Alergeno';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-usuario',
  templateUrl: './update-usuario.component.html',
  styleUrls: ['./update-usuario.component.css']
})
export class UpdateUsuarioComponent implements OnInit {

  constructor(private router: Router, private tokenStorage: TokenStorage, private usuarioService: UsuarioService, private alergenoService: AlergenoService, private usuarioAlergenoService: UsuarioAlergenoService) { }

  //Alergenos Marcados
  selectedAlergenos:any = [];
  modificando=false;

  alergenos!: Observable<alergeno[]>;
  alergenosUsuario!: Observable<usuario_Alergeno[]>;

  user: usuario = {
    id_usuario: 0,
    username: '',
    password: '',
    email: '',
    nombre: '',
    apellidos: '',
  }


  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.user = this.tokenStorage.getUser();
      this.obtenerAlergenos();
      this.obtenerAlergenosDelUsuario();
    }
  }

  //Alergenos checked
  OnCheckboxSelect(alergeno:alergeno, event:any) {
    if (event.target.checked === true) {
      this.selectedAlergenos.push(alergeno);
      console.log('Selected Ids ', this.selectedAlergenos);
    }
    if (event.target.checked === false) {
      this.selectedAlergenos = this.selectedAlergenos.filter((data: alergeno) => data !== alergeno);
    }
  }

  // Modificar nombre i apellido (Timeout para solucionar error de sincronización)
  modificarDatos() {
    // NO TIMEOUT
    this.user = this.usuarioService.updateUsuarioData(this.user.id_usuario ,this.user);

    setTimeout(() => {
      // TIMEOUT
      this.tokenStorage.saveUser(this.user);
    }, 1000);
  }

  //Obtener Alergenos de la BD
  obtenerAlergenos() {
    this.alergenos = this.alergenoService.getAlergenos();
  }

  obtenerAlergenosDelUsuario() {
    this.alergenosUsuario = this.usuarioAlergenoService.getAlergenosPorUsuario(this.user.id_usuario);
  }

  updateAlergenosUsuario() {
    this.usuarioAlergenoService.updateAlergenosUsuario(this.user.id_usuario, this.selectedAlergenos);
    this.modificando = true;
    setTimeout(() => {
      this.volver();
    }, 4000);
  }

    //Metodo para volver al usuario
    volver(){
      this.router.navigate(['/usuario/update']).then(() => { window.location.reload() });
    }


}
