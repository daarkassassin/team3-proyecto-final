import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { usuario } from '../../../models/usuario';
import { UsuarioService } from '../../../services/usuario/usuario.service';

@Component({
  selector: 'app-list-usuario',
  templateUrl: './list-usuario.component.html',
  styleUrls: ['./list-usuario.component.css']
})

export class ListUsuarioComponent implements OnInit {
  usuarios!: Observable<usuario[]>;

  constructor(private UsuarioService: UsuarioService, private router: Router) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData() {
    this.usuarios = this.UsuarioService.getUsuarioList();
  }

  deleteUsuario(id_usuario: number) {
    this.UsuarioService.deleteUsuario(id_usuario)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  updateUsuario(id_usuario: number){
    this.router.navigate(['Usuario/update', id_usuario]);
  }

}
