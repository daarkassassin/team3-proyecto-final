import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';
import { usuario } from '../../../models/usuario';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { TokenStorage } from '../../../services/auth/token-storage.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})



export class RegisterComponent implements OnInit {
  //angForm!: FormGroup;
  angForm!: any; //Con any no da el error "Object is possibly 'null'.ngtsc(2531)"
  debug = false;

  constructor(private authService: AuthService, private router: Router, private location: Location, private tokenStorage: TokenStorage, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      username: [Validators.minLength(4), Validators.required],
      email: [Validators.email, Validators.required],
      password: [Validators.minLength(8),Validators.required],
      checkbox: [false, Validators.requiredTrue ],
    });
  }

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  user: usuario = {
    id_usuario: 0,
    username: '',
    password: '',
    email: '',
    nombre: '',
    apellidos: '',
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/catalogo']);
    }

  }

  onRegister() {
    if(this.angForm.status === "INVALID"){
      this.isLoginFailed = true;
    }else{
      this.authService.registerUser(this.user.username,this.user.email, this.user.password)
      .subscribe(
        data => {
          console.log(data);
          this.authService.loginuser(this.user.username, this.user.password).subscribe(resp =>
            {
              this.tokenStorage.saveToken(resp.headers.get('Authorization'));
              this.authService.getCurrentUser(this.user.username).subscribe( data => {
                this.user = data, this.user.password = "*****";
                this.tokenStorage.saveUser(this.user);
                //this.router.navigate(['/catalogo']);
                this.reloadPage();
              });
              this.isLoginFailed = false;
              this.isLoggedIn = true;
            },
            error=>{
              console.log(error);
              this.errorMessage = "Error: Usuario o contraseña incorrectos";
              this.isLoginFailed = true;
            });
        },
        res => {
          this.errorMessage = res.error;
          this.isLoginFailed = true;
        }
      );
    }

  }

  reloadPage() {
    window.location.reload();
  }

}
