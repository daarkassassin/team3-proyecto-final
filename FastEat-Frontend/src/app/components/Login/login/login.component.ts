import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { AuthService } from '../../../services/auth/auth.service';
import { usuario } from '../../../models/usuario';
import { TokenStorage } from '../../../services/auth/token-storage.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  //angForm!: FormGroup;
  angForm!: any; //Con any no da el error "Object is possibly 'null'.ngtsc(2531)"
  debug = false;

  constructor(private authService: AuthService, private router: Router, private location: Location, private tokenStorage: TokenStorage, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      username: ['', Validators.required ],
      password: ['', Validators.required ],
    });
  }

  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';

  user: usuario = {
    id_usuario: 0,
    username: '',
    password: '',
    email: '',
    nombre: '',
    apellidos: '',
  }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(['/catalogo']);
    }
  }

  onLogin() {
    if(this.angForm.status == "INVALID"){
      this.isLoginFailed = true;
      return null;
    }else{
      return this.authService.loginuser(this.user.username, this.user.password)
      .subscribe(resp =>
        {
          this.tokenStorage.saveToken(resp.headers.get('Authorization'));
          this.authService.getCurrentUser(this.user.username).subscribe( data => {
            this.user = data, this.user.password = "*****";
            this.tokenStorage.saveUser(this.user);
            //this.router.navigate(['/home']);
            this.reloadPage();
          });
          this.isLoginFailed = false;
          this.isLoggedIn = true;
        },
        error=>{
          console.log(error);
          this.errorMessage = "Error: Usuario o contraseña incorrectos";
          this.isLoginFailed = true;
        });
    }
  }

  reloadPage() {
    window.location.reload();
  }





}
