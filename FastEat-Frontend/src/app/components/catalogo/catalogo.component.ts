import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TokenStorage } from '../../services/auth/token-storage.service';
import { categoria } from 'src/app/models/categoria';
import { CategoriaService } from '../../services/categoria/categoria.service';
import { item } from 'src/app/models/item';
import { Observable, Subscription } from 'rxjs';
import { ItemService } from 'src/app/services/item/item.service';
import { map } from 'rxjs/operators';
import { CarritoStorage } from '../../services/carrito/carrito-storage.service';
import { MessageService } from 'src/app/services/message.service';

const AUTH_API = 'http://localhost/';



@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {
  subscription!: Subscription;

  //Esta el catalogo cargado?
  cargado = false;

  constructor(private router: Router,
    private tokenStorage: TokenStorage,
    private carritoStorage: CarritoStorage,
    private http: HttpClient,
    private categoriaService: CategoriaService,
    private itemService: ItemService,
    private messageService: MessageService) {
      this.subscription = this.messageService.getMessage().subscribe(message => {
        if (message) {
          if(message.text == 'PlatoEliminado'){
            console.log(message);
            this.articulos = message.data;
          }

        }
      });
    }

  //Observables de categorias y articulos
  categorias!: Observable<categoria[]>;
  articulos!: Observable<item[]>;




  //Id de la categoria y nombre del articulo buscado
  id = "";
  nombre = "";

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.obtenerArticulos();
      this.obtenerCategorias();
    }
  }

  //Obtener Categorias de la BD
  obtenerCategorias() {
    this.categorias = this.categoriaService.getCategoriasList();
  }

  //Obtener Articulos de la BD
  obtenerArticulos() {
    this.articulos = this.itemService.getArticulosList();
    this.articulos = this.articulos.pipe(map(items => items.filter(item => Boolean(item.status) == Boolean(true), this.cargado = true ) ) )
  }

  //Filtrar por categoria
  onIdChangeFilter($event: any) {
    this.cargado = false;
    if (Number(this.id) != 0) {
      // FILTRO
      this.articulos = this.articulos.pipe(map(items => items.filter(item => Number(item.categoria.id_categoria) == Number(this.id) ) ) )
      this.buscarPorNombre();
    } else {
      //NO FILTRO
      this.obtenerArticulos();
      this.buscarPorNombre();
    }
  }

  //Evento - Filtrar por nombre
  onNameChangeFilter($event: any) {
    this.cargado = false;
    this.buscarPorNombre()
  }

  //Filtrar Articulos por nombre
  buscarPorNombre() {
    this.articulos = this.articulos.pipe(map(items => items.filter(item => item.nombre.toLowerCase().includes( this.nombre.toLowerCase() )  ) ) )
  }

  //Ver 1 plato en especifico
  verPlato(id_plato: number){
    this.router.navigate(['/articulo', id_plato]);
  }

  //Añadir 1 articulo al carrito
  addCarrito(articulo:item ){
    this.carritoStorage.saveCarrito(articulo);

  }

  //Metodo para recargar el catalogo
  volver(){
    this.router.navigate(['/catalogo']).then(() => { window.location.reload() });
  }


}
