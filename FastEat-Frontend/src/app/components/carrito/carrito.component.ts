import { Component, OnInit, Injectable } from '@angular/core';
import { share } from 'rxjs/operators';
import { Observable, of, Subject } from 'rxjs';
import { item } from 'src/app/models/item';
import { CarritoStorage } from 'src/app/services/carrito/carrito-storage.service';
import { franjaHoraria } from 'src/app/models/franjaHoraria';
import { FranjaHorariaService } from 'src/app/services/franjaHoraria/franja-horaria.service';
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import { usuario } from 'src/app/models/usuario';
import { PedidoService } from 'src/app/services/pedido/pedido.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})

export class CarritoComponent implements OnInit {

  hayErrores!: Boolean;

  array = this.carritoStorage.getCarrito();
  articulos = of(this.array);
  precio = 0;

  constructor(private router: Router, private carritoStorage: CarritoStorage, private franjaHorariaService: FranjaHorariaService, private tokenStorage: TokenStorage, private pedidoService: PedidoService) { }

  modificando = false;

  user!: usuario;

  franjaHorariaOcupacionActual!: string;
  franjaHorariaID!: number;
  franjasHorarias!: Observable<franjaHoraria[]>;
  contador!: number;

  ngOnInit(): void {
    this.carritoStorage.getContador();
    this.obtenerFranjasHorarias();
    this.updateCarrito();
    this.calcularPrecio();
    this.carritoStorage.watchStorage().subscribe((data:string) => {
      this.updateCarrito();
      this.calcularPrecio();
    })
  }

  obtenerFranjasHorarias() {
    this.franjasHorarias = this.franjaHorariaService.getFranjasHorarias();
  }

  //Calcular precio total del carrito
  calcularPrecio(){
    if(Number(this.array.lenght) == 0){
      this.precio = 0;
    }else{
      this.precio = 0;
      this.array.forEach((element: item) => {
        console.log(element);
        this.precio = this.precio + element.precio;
      });
    }
  }

  //Eliminar item del carrito
  eliminarArticulo(Item: item){
    this.carritoStorage.deleteItemCarrito(Item);
    this.contador = this.carritoStorage.getContador();
  }

  //Añadir item al carrito
  añadirCarrito(articulo:any){
    this.carritoStorage.saveCarrito(articulo);
    this.contador = this.carritoStorage.getContador();
  }

  //Vaciar Carrito
  vaciarCarrito(){
    this.carritoStorage.clearCarrito();
    this.contador = this.carritoStorage.getContador();
  }

  //Actualizar Carrito
  updateCarrito(){
    this.array = this.carritoStorage.getCarrito();
    this.articulos = of(this.array);
    this.contador = this.carritoStorage.getContador();
  }

  //Obtener el id de la franja horaria al hacer click
  selectedFranjaHoraria(event: any) {
    this.franjaHorariaID = event.target.id;
    this.franjaHorariaOcupacionActual = event.target.value;
  }

  realizarPedido() {

    this.user = this.tokenStorage.getUser();
    this.pedidoService.guardarPedido(this.franjaHorariaID, this.user.id_usuario, this.precio);
    this.modificando = true;

    setTimeout(() => {
      this.vaciarCarrito();
      this.volver();
    }, 1500);
  }

  //Metodo para volver al catalogo
  volver(){
    this.router.navigate(['/catalogo']).then(() => { window.location.reload() });
  }


}
