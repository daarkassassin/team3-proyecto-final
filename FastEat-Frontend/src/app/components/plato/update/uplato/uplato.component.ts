import { Component, ElementRef, OnInit, ViewChild, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import { categoria } from 'src/app/models/categoria';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { alergeno } from 'src/app/models/alergeno';
import { AlergenoService } from 'src/app/services/alergeno/alergeno.service';
import { item } from 'src/app/models/item';
import { ItemService } from 'src/app/services/item/item.service';

@Component({
  selector: 'app-uplato',
  templateUrl: './uplato.component.html',
  styleUrls: ['./uplato.component.css']
})
export class UplatoComponent implements OnInit {
  @ViewChild('inputimage') iLmg!: ElementRef;
  @ViewChild('labelimage') lImg!: ElementRef;

  //articulo  + categoria
  @Input() item!: item;

  //angForm y debug
  angForm!: any; //Con any no da el error "Object is possibly 'null'.ngtsc(2531)"
  debug = false;

  //Observables de categorias y articulos
  categorias!: Observable<categoria[]>;
  alergenos!: Observable<alergeno[]>;

  //Imagen File
  imagenEntrada!: File;

  //Se esta modificando?
  modificando = false;

  //Alergenos Marcados
  selectedAlergenos:any = [];

  constructor(private router: Router,
    private tokenStorage: TokenStorage,
    private categoriaService: CategoriaService,
    private itemService: ItemService,
    private alergenoService: AlergenoService,
    private fb: FormBuilder) {
      this.createForm();
    }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.obtenerCategorias();
      this.obtenerAlergenos();
      this.item.imagen = '';
    }

  }
  //Alergenos checked
  OnCheckboxSelect(alergeno:alergeno, event:any) {
    if (event.target.checked === true) {
      this.selectedAlergenos.push(alergeno);
      console.log('Selected Ids ', this.selectedAlergenos);
    }
    if (event.target.checked === false) {
      this.selectedAlergenos = this.selectedAlergenos.filter((data: alergeno) => data !== alergeno);
    }
  }

  //Metodo para cambiar el label del input de la imagen
  changeLabel(){
    var filename = this.iLmg.nativeElement.value.match(/[^\\/]*$/)[0];
    this.lImg.nativeElement.innerHTML = filename
  }

  //Metodo para validar el formulario
  createForm() {
    this.angForm = this.fb.group({
      nombre: ['', Validators.required ],
      categoria: ['', Validators.required],
      descripcion: [Validators.required, Validators.maxLength(200)],
      precio: [Validators.required ],
      image: ['',Validators.required ]
    });
  }

  //Obtener Categorias de la BD
  obtenerCategorias() {
    this.categorias = this.categoriaService.getCategoriasList();
  }

  //Obtener Alergenos de la BD
  obtenerAlergenos() {
    this.alergenos = this.alergenoService.getAlergenos();
  }

  //Modificamos el articulo
  updateItem(){
    if(this.angForm.status == "VALID"){
      //Mostramos  el spinner de modificar
      this.modificando = true;

      //Convertimos la imagen a Base64.
      var binary = new FileReader();
      let imgb64: any;
      binary.addEventListener("load", function () {
        imgb64 = this.result;
      }, false);
      binary.readAsDataURL(this.imagenEntrada)

      //Modificamos el articulo
      setTimeout(() => {
        this.item.imagen = imgb64.replace(/^data:image\/[a-z]+;base64,/, '');
        setTimeout(() => {
          this.itemService.updateArticulo(this.item, this.selectedAlergenos);
          setTimeout(() => {
            this.modificando = false;
            this.volver();
          }, 2000);
        }, 500);
      }, 500);

    }

  }

  //Metodo para volver al catalogo
  volver(){
    //this.router.navigate(['/catalogo']).then(() => { window.location.reload() });
    window.location.reload();
  }




}
