import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UplatoComponent } from './uplato.component';

describe('UplatoComponent', () => {
  let component: UplatoComponent;
  let fixture: ComponentFixture<UplatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UplatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UplatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
