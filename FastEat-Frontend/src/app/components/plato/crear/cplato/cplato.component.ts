import { Component, ElementRef, NgModule, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder,  Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import { categoria } from 'src/app/models/categoria';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { alergeno } from 'src/app/models/alergeno';
import { AlergenoService } from 'src/app/services/alergeno/alergeno.service';
import { item } from 'src/app/models/item';
import { ItemService } from 'src/app/services/item/item.service';

@Component({
  selector: 'app-cplato',
  templateUrl: './cplato.component.html',
  styleUrls: ['./cplato.component.css']
})
export class CplatoComponent implements OnInit {
  @ViewChild('inputimage') iLmg!: ElementRef;
  @ViewChild('labelimage') lImg!: ElementRef;

  //angForm y debug
  angForm!: any; //Con any no da el error "Object is possibly 'null'.ngtsc(2531)"
  debug = false;

  //Observables de categorias y articulos
  categorias!: Observable<categoria[]>;
  alergenos!: Observable<alergeno[]>;
  alergenox!: any;

  //articulo  + categoria
  item: item ={
    id_item: 0,
    nombre: '',
    descripcion: '',
    imagen: '',
    precio: 0,
    status: true,
    categoria:{
      id_categoria: 1,
      nombre: '',
      descripcion: ''
    }
  }

  //Imagen de entrada
  imagenEntrada!: File;

  //Se esta creando?
  creando = false;

  //Alergenos Marcados
  selectedAlergenos:any = [];


  constructor(private router: Router,
    private tokenStorage: TokenStorage,
    private categoriaService: CategoriaService,
    private itemService: ItemService,
    private alergenoService: AlergenoService,
    private fb: FormBuilder) {
      this.createForm();
    }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.obtenerCategorias();
      this.obtenerAlergenos();
    }
  }
  //Alergenos checked
  OnCheckboxSelect(alergeno:alergeno, event:any) {
    if (event.target.checked === true) {
      this.selectedAlergenos.push(alergeno);
      console.log('Selected Ids ', this.selectedAlergenos);
    }
    if (event.target.checked === false) {
      this.selectedAlergenos = this.selectedAlergenos.filter((data: alergeno) => data !== alergeno);
    }
  }


  //Metodo para cambiar el label del input de la imagen
  changeLabel(){
    var filename = this.iLmg.nativeElement.value.match(/[^\\/]*$/)[0];
    this.lImg.nativeElement.innerHTML = filename
  }

  //Metodo para validar el formulario
  createForm() {
    this.angForm = this.fb.group({
      nombre: ['', Validators.required ],
      categoria: ['', Validators.required],
      descripcion: [Validators.required, Validators.maxLength(200)],
      precio: [Validators.required ],
      image: ['',Validators.required ]
    });
  }

  //Obtener Categorias de la BD
  obtenerCategorias() {
    this.categorias = this.categoriaService.getCategoriasList();
  }

  //Obtener Alergenos de la BD
  obtenerAlergenos() {
    this.alergenos = this.alergenoService.getAlergenos();
  }

  crearItem(){
    if(this.angForm.status == "VALID"){
      //Mostramos el spiner de espera
      this.creando = true;

      //Obtenemos la imagen
      var binary = new FileReader();
      let imgb64: any;
      binary.addEventListener("load", function () {
        imgb64 = this.result;
      }, false);
      binary.readAsDataURL(this.imagenEntrada)

      //Creamos el nuevo articulo y volvemos al catalogo
      setTimeout(() => {
        this.item.imagen = imgb64.replace(/^data:image\/[a-z]+;base64,/, '');
        setTimeout(() => {
          this.itemService.createArticulo(this.item, this.selectedAlergenos);
          setTimeout(() => {
            this.creando = false;
            this.volver();
          }, 2000);
        }, 500);
      }, 500);



    }

  }
  //Metodo para volver al catalogo
  volver(){
    this.router.navigate(['/catalogo']).then(() => { window.location.reload() });
  }




}
