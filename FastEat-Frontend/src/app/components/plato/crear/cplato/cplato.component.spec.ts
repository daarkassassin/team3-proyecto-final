import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CplatoComponent } from './cplato.component';

describe('CplatoComponent', () => {
  let component: CplatoComponent;
  let fixture: ComponentFixture<CplatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CplatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CplatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
