import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { categoria } from 'src/app/models/categoria';
import { item } from 'src/app/models/item';
import { item_Alergeno } from 'src/app/models/item_Alergeno';
import { CarritoStorage } from 'src/app/services/carrito/carrito-storage.service';
import { ItemService } from 'src/app/services/item/item.service';
import { ItemAlergenoService } from 'src/app/services/item_Alergeno/item-alergeno.service';
import { MessageService } from '../../services/message.service';




@Component({
  selector: 'app-plato',
  templateUrl: './plato.component.html',
  styleUrls: ['./plato.component.css']
})
export class PlatoComponent implements OnInit {

  //Id del articulo - Entrada
  private idArticulo!: number;

  //Flag que controla el mostrar o no los alergenos
  control!: boolean;

  //Alergenos
  alergenos!: Observable<item_Alergeno[]>;

  //Articulo Que estamos viendo
  articulo: item = {
    id_item: 0,
    nombre: "",
    descripcion: "",
    imagen: null,
    precio: 0,
    status: true,
    categoria!: new categoria,
  }

  //Se esta eliminando?
  eliminando = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private itemService: ItemService,
    private carritoStorage: CarritoStorage,
    private item_AlergenoService: ItemAlergenoService,
    private messageService: MessageService ) { }

  ngOnInit(): void {
    this.getIdArticulo(); //Obtenemos el id del articulo
    this.obtenerArticulo(); //Obtenemos el articulo a mostrar
    this.obtenerAlergenosConArticulosFiltrandoArticulo(); //Obtenemos los alergenos de este articulo
  }

  //Obtener el id del articulo
  getIdArticulo() {
    this.activatedRoute.params.subscribe(params => {this.idArticulo = params['id'] } );
  }

  //Obtener articulo
  obtenerArticulo() {
    this.itemService.getArticulo(this.idArticulo).subscribe( data => {
      this.articulo.id_item = data.id_item;
      this.articulo.nombre = data.nombre;
      this.articulo.descripcion = data.descripcion;
      this.articulo.imagen = data.imagen;
      this.articulo.precio = data.precio;
      this.articulo.status = data.status;
      this.articulo.categoria = data.categoria;
    });
  }

  //obtener articulo y desactivarlo  " Eliminarlo "
  obtenerArticuloForDelete() {
      this.eliminando = true;
      this.articulo.status = false;
      this.itemService.deleteArticulo(this.idArticulo, this.articulo);

      // Forzamos a que se actualize el catalogo y volvemos al catalogo
      this.itemService.getArticulosList().subscribe(data => {
        this.messageService.sendMessage('PlatoEliminado',data);
        this.volver()
      });
  }

  //Filtrar alergenos con el plato adecuado
  obtenerAlergenosConArticulosFiltrandoArticulo() {
    this.alergenos = this.item_AlergenoService.getArticulosAlergenos();
    this.alergenos = this.alergenos.pipe(map(items => items.filter(item => Number(item.item.id_item) == Number(this.idArticulo) ) ) )
    this.alergenos.subscribe((response) => {
      if (response.length == 0) {
        this.control = false;
      } else {
        this.control = true;
      } }

    )




  }

  //Metodo para volver al catalogo
  volver(){
    this.router.navigate(['/catalogo']).then(() => { window.location.reload() });
  }

  //Añadir 1 articulo al carrito
  addCarrito(articulo:item ){
    this.carritoStorage.saveCarrito(articulo);

  }

}
