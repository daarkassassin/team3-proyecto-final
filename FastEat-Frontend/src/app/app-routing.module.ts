import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './components/Info/info/info.component';
import { ForgotComponent } from './components/Login/forgot/forgot.component';
import { LoginComponent } from './components/Login/login/login.component';
import { RegisterComponent } from './components/Login/register/register.component';
import { ListUsuarioComponent } from './components/Usuario/list-usuario/list-usuario.component';
import { AboutComponent } from './components/about/about.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { PlatoComponent } from './components/plato/plato.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { UpdateUsuarioComponent } from './components/Usuario/update-usuario/update-usuario.component';
import { CplatoComponent } from './components/plato/crear/cplato/cplato.component';
import { UplatoComponent } from './components/plato/update/uplato/uplato.component';

const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: 'catalogo', component: CatalogoComponent },
  { path: 'articulo/:id', component: PlatoComponent },
  { path: 'pedidos', component: PedidosComponent },
  { path: 'usuario/list', component: ListUsuarioComponent },
  { path: 'usuario/update', component: UpdateUsuarioComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot', component: ForgotComponent },
  { path: '404', component: InfoComponent },
  { path: '', pathMatch:'full', redirectTo: 'catalogo'},
  { path: '**', pathMatch:'full', redirectTo: '404'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

