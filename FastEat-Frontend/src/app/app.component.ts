import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit} from '@angular/core';

const COLOR_KEY = 'color-user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ReservaComedor-Frontend';
  color= 'bg-theme2';

  constructor(@Inject(DOCUMENT) private document: Document){ }

  ngOnInit(): void {

    //Cargamos el color por defecto de la pagina, o el del usuario.
    if(localStorage.getItem(COLOR_KEY) === null ){
      window.localStorage.setItem(COLOR_KEY, JSON.stringify('bg-theme2'));
      this.document.body.classList.add('bg-theme2');
    }else{
      this.color = JSON.parse(localStorage.getItem(COLOR_KEY)!);
      this.document.body.classList.add(''+this.color+'');
    }

  }



}
