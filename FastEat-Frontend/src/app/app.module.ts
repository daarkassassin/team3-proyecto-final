import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/Login/login/login.component';
import { RegisterComponent } from './components/Login/register/register.component';
import { ForgotComponent } from './components/Login/forgot/forgot.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfoComponent } from './components/Info/info/info.component';
import { NotFoundComponent } from './components/Info/not-found/not-found.component';
import { HeaderComponent } from './components/Layout/header/header.component';
import { FooterComponent } from './components/Layout/footer/footer.component';
import { ColorComponent } from './components/Layout/color/color.component';
import { ListUsuarioComponent } from './components/Usuario/list-usuario/list-usuario.component';
import { UpdateUsuarioComponent } from './components/Usuario/update-usuario/update-usuario.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AboutComponent } from './components/about/about.component';
import { PlatoComponent } from './components/plato/plato.component';
import { CarritoComponent } from './components/carrito/carrito.component';
import { PedidosComponent } from './components/pedidos/pedidos.component';
import { CatalogoComponent } from './components/catalogo/catalogo.component';
import { CplatoComponent } from './components/plato/crear/cplato/cplato.component';
import { UplatoComponent } from './components/plato/update/uplato/uplato.component';
import { FileInputValueAccessor } from "./services/file-input.accessor";
import { ViewComponent } from './components/pedidos/view/view.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    InfoComponent,
    NotFoundComponent,
    HeaderComponent,
    FooterComponent,
    ColorComponent,
    ListUsuarioComponent,
    UpdateUsuarioComponent,
    AboutComponent,
    PlatoComponent,
    CarritoComponent,
    CatalogoComponent,
    PedidosComponent,
    CplatoComponent,
    UplatoComponent,
    FileInputValueAccessor,
    ViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,              //necesario para conectar con API
    HttpClientModule,         //necesario para conectar con API
    BrowserModule,            //necesario para validar
    ReactiveFormsModule       //necesario para validar
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
