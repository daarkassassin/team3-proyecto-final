import { pedido } from "./pedido";
import { item } from "./item";

export class Pedido_Item{
  id!: Number;
  pedido!: pedido;
  item!: item;
}
