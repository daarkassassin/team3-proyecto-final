import { usuario } from "./usuario";
import { franjaHoraria } from "./franjaHoraria";

export class pedido{
	id_pedido!: number;
  fecha!: Date ;
  precioTotal!: number;
  iva!: number;
  usuario!: usuario;
  franjaHoraria!: franjaHoraria;
}
