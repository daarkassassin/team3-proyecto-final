import { alergeno } from "./alergeno";
import { item } from "./item";

export class item_Alergeno{
  id!: Number;
  item!: item;
  alergeno!: alergeno;
}
