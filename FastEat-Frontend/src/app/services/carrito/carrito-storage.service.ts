import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { item } from 'src/app/models/item';

const CARRITO_KEY = 'carrito-user';

@Injectable({
  providedIn: 'root'
})
export class CarritoStorage {
  //Array de articulos y storageSub para detectar cambios en el local storage
  private Items = new Array<{Item: item}>();
  private storageSub= new Subject<String>();

  //Contador de items en el carrito
  private itemsAlmacenados = 0;

  //Contador Total a pagar
  private itemsPrecioSuma = 0;

  //Constructor
  constructor() {
    this.Items = this.getCarrito();
    if(this.Items == null){
      this.itemsAlmacenados = 0;
    } else {
      this.itemsAlmacenados = this.Items.length;
    }

  }

  //Observable que usaremos para detectar cambios
  watchStorage(): Observable<any> {
    return this.storageSub.asObservable();
  }

  //Metodo que guarda el carrito en el localstorage
  public saveCarritoLocalStorage(Entrada: any) {
    window.localStorage.removeItem(CARRITO_KEY);
    window.localStorage.setItem(CARRITO_KEY, JSON.stringify(Entrada));
    this.storageSub.next('changed');
  }

  //Metodo para añadir 1 item al carrito
  public saveCarrito(Entrada: any) {
    this.Items = this.getCarrito();
    if (this.Items == null) {
      this.Items = new Array<{Item: item}>();
      this.Items.push(Entrada);
    }else{
      this.Items.push(Entrada);
    }
    this.itemsAlmacenados++;
    this.saveCarritoLocalStorage(this.Items);
  }

  //Metodo para eliminar un articulo  del carrito
  public deleteItemCarrito(Entrada: any) {
    this.Items = this.getCarrito();
    var flag = true;
    for( var i = 0; i < this.Items.length; i++){
      if ( JSON.stringify(this.Items[i]) === JSON.stringify(Entrada) && flag) {
        this.Items.splice(i, 1);
        this.itemsAlmacenados--;
        flag = false;
      }
    }
    this.saveCarritoLocalStorage(this.Items);
  }

  //Metodo para obtener el carrito
  public getCarrito(): any {
    return JSON.parse(localStorage.getItem(CARRITO_KEY)!);
  }

  //Metodo para vaciar el carrito
  public clearCarrito() {
    window.localStorage.removeItem(CARRITO_KEY);
    this.storageSub.next('changed');
    this.itemsAlmacenados = 0;

    this.Items = new Array<{Item: item}>();
    this.saveCarritoLocalStorage(this.Items);
  }

  public getContador(): any {
    return this.itemsAlmacenados;
  }





}


