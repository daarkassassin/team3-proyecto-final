import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from '../auth/token-storage.service';
import { pedido } from 'src/app/models/pedido';
import { item } from 'src/app/models/item';
import { NgModule } from '@angular/core';
import { Pedido_Item } from 'src/app/models/pedido_item';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})
export class PedidoItemService {

  error = 0;

  pedidoItem: Pedido_Item ={
    id: 0,
    pedido: {
      id_pedido: 0,
      fecha: new Date(),
      precioTotal: 0,
      iva: 0,
      usuario:{
        id_usuario: 0,
        username: '',
        password: '',
        email: '',
        nombre: '',
        apellidos: '',
      },
      franjaHoraria:{
        id_franjahoraria: 0,
        horaInicio: '',
        horaFinal: '',
        ocupacionMaxima: 0,
        ocupacionActual: 0
      }
    },
    item: {
      id_item: 0,
      nombre: '',
      descripcion: '',
      imagen: '',
      precio: 0,
      status: true,
      categoria:{
        id_categoria: 1,
        nombre: '',
        descripcion: ''
      }
    }
  }

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  getPedidosItemsList(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    return this.http.get<any>(GlobalConstantsService.apiURL + '/pedidos_Items', {headers})
  }

  crearPedidoItem(data: pedido, selectedArticulos:any) {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    this.error = 0;

    // hacemos un post por cada uno de los articulos existentes
    for(let articulo of selectedArticulos){

      let pedido_item = {
        id: 0,
        pedido: data,
        item: articulo
      }

      this.http.post<any>(GlobalConstantsService.apiURL + "/pedido_Item", pedido_item, {headers}).subscribe(data =>
        data,
        error => {
          if (this.error != 1) {
            alert("¡Error! El pedido no se ha creado correctamente.");
          }
          this.error = 1;
        }
      );
    }
  }


}
