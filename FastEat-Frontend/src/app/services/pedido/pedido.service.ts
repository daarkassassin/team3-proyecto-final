import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from '../auth/token-storage.service';
import { pedido } from 'src/app/models/pedido';
import { usuario } from 'src/app/models/usuario';
import { franjaHoraria } from 'src/app/models/franjaHoraria';
import { NgModule } from '@angular/core';
import { CarritoStorage } from '../carrito/carrito-storage.service';
import { PedidoItemService } from '../pedido_item/pedido-item.service';
import { FranjaHorariaService } from '../franjaHoraria/franja-horaria.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';


@Injectable({
  providedIn: 'root'
})
export class PedidoService {

  franjaHoraria: franjaHoraria = {
    id_franjahoraria: 0,
    horaInicio: '',
    horaFinal: '',
    ocupacionMaxima: 0,
    ocupacionActual: 0
  }

  pedido: pedido ={
    id_pedido: 0,
    fecha: new Date(),
    precioTotal: 0,
    iva: 0,
    usuario:{
      id_usuario: 0,
      username: '',
      password: '',
      email: '',
      nombre: '',
      apellidos: '',
    },
    franjaHoraria:{
      id_franjahoraria: 0,
      horaInicio: '',
      horaFinal: '',
      ocupacionMaxima: 0,
      ocupacionActual: 0
    }
  }

  constructor(private tokenStorage: TokenStorage, private http: HttpClient, private carritoStorage: CarritoStorage, private pedidoItemService: PedidoItemService, private franjaHorariaService: FranjaHorariaService) { }

  getPedidosList(){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    return this.http.get<any>(GlobalConstantsService.apiURL + '/pedidos', {headers})
  }

  // Guardamos el pedido i luego utilizamos un metodo que guarda los articulos en el carrito
  guardarPedido(idFranjaHoraria: number, idUsuario: number, precio: number) {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    this.pedido.fecha = new Date();
    this.pedido.precioTotal = precio;
    this.pedido.usuario.id_usuario = idUsuario;
    this.pedido.franjaHoraria.id_franjahoraria = idFranjaHoraria;

    this.http.post<pedido>(GlobalConstantsService.apiURL + '/pedido', this.pedido, {headers}).subscribe(data =>
        this.pedidoItemService.crearPedidoItem(data, this.carritoStorage.getCarrito()),
        error => {
          alert("¡Error! El pedido no se ha creado correctamente.");
        }
    );

    this.franjaHorariaService.getFranjaHorariaPorId(idFranjaHoraria).subscribe( data => {
      this.franjaHoraria.id_franjahoraria = data.id_franjahoraria;
      this.franjaHoraria.horaInicio = data.horaInicio;
      this.franjaHoraria.horaFinal = data.horaFinal;
      this.franjaHoraria.ocupacionActual = data.ocupacionActual + 1;
      this.franjaHoraria.ocupacionMaxima = data.ocupacionMaxima;
      this.franjaHorariaService.modificarFranjaHoraria(this.franjaHoraria, this.franjaHoraria.id_franjahoraria);
    });

  }

}
