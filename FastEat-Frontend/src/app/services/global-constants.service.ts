import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalConstantsService {

  public static apiURL: string = "http://localhost/api";
  public static authURL: string = "http://localhost/";

  constructor() { }
}
