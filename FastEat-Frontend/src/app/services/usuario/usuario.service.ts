import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from '../auth/token-storage.service';
import { usuario } from 'src/app/models/usuario';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})

export class UsuarioService {

  user: usuario = {
    id_usuario: 0,
    username: '',
    password: '',
    email: '',
    nombre: '',
    apellidos: '',
  }

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  getUsuarioList(): Observable<any> {
    return this.http.get( GlobalConstantsService.apiURL+'/usuarios');
  }

	createUsuario(usuario: Object): Observable<object> {
    return this.http.post(GlobalConstantsService.apiURL+'/usuario', usuario);
  }

	getUsuario(id: number): Observable<any> {
    return this.http.get(GlobalConstantsService.apiURL+'/usuario/${id}');
  }

  // Update del usuario que modifica todo menos la password
	updateUsuarioData(id: number, value: usuario): usuario {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders({"Authorization": token, "Content-Type": "application/json"})

    this.http.put<usuario>(GlobalConstantsService.apiURL+ '/usuario/updateData/' + id, value, {headers}).subscribe(data => {
      this.user.id_usuario = data.id_usuario;
      this.user.username = data.username;
      this.user.password = "*****";
      this.user.email = data.email;
      this.user.nombre = data.nombre;
      this.user.apellidos = data.apellidos;
    });
    return this.user;
  }

	deleteUsuario(id: number): Observable<any> {
    return this.http.delete(GlobalConstantsService.apiURL+'/usuario/${id}', { responseType: 'text'});
  }

}
