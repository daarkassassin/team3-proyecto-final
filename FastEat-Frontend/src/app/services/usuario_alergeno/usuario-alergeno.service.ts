import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { TokenStorage } from 'src/app/services/auth/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioAlergenoService {

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  private baseUrl = 'http://localhost/api';

  getAlergenosPorUsuario(id: number): Observable<any>{
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<any>(this.baseUrl + '/usuarios_alergenos/usuario/' + id, {headers})
  }

  updateAlergenosUsuario(id: number, selectedAlergenos: any){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders({"Authorization": token, "Content-Type": "application/json"});

    this.deleteAlergenosUsuarioByID(id);
    this.createAlergenosUsuario(id, selectedAlergenos);

  }

  deleteAlergenosUsuarioByID(id: number){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    this.http.delete<any>(this.baseUrl + "/usuario_alergenos/usuario/" + id, {headers}).subscribe(data => data);
  }

  createAlergenosUsuario(id: number, selectedAlergenos: any) {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    for(let alergeno of selectedAlergenos){
      let usuario_alergeno = {
        id: 0,
        usuario: {id_usuario: id},
        alergeno: alergeno
      }
      this.http.post<any>(this.baseUrl + "/usuario_alergeno", usuario_alergeno, {headers}).subscribe(data => data);

    }

  }

}
