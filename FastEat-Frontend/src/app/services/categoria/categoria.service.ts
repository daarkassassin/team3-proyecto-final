import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from '../auth/token-storage.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private baseUrl = 'http://localhost/api';

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  getCategoriasList(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<any>(GlobalConstantsService.apiURL + '/categorias', {headers})
  }

}
