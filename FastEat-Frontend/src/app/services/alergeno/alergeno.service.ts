import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';



@Injectable({
  providedIn: 'root'
})
export class AlergenoService {

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  getAlergenos(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<any>(GlobalConstantsService.apiURL + '/alergenos', {headers})
  }

}
