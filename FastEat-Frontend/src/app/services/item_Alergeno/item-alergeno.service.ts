import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { item } from 'src/app/models/item';
import { alergeno } from 'src/app/models/alergeno';
import { item_Alergeno } from 'src/app/models/item_Alergeno';
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})

export class ItemAlergenoService {

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  crearArticulosAlergenos(data: item, selectedAlergenos:any){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    for(let o of selectedAlergenos){
      let articulo_Alergeno = {
        id: 0,
        item:data,
        alergeno: o
      }

      this.http.post<any>(GlobalConstantsService.apiURL + "/item_Alergeno", articulo_Alergeno, {headers}).subscribe(data =>{
        data;
        //console.log(data);
      })

    }

  }

  deleteArticulosAlergenosByItemID(id: number){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    this.http.delete<any>(GlobalConstantsService.apiURL + "/item_Alergeno/item/delete/"+id, {headers}).subscribe(result => result);

  }

  getArticulosAlergenos(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<any>(GlobalConstantsService.apiURL + '/items_Alergenos', {headers})
  }

}


