import { TestBed } from '@angular/core/testing';

import { ItemAlergenoService } from './item-alergeno.service';

describe('ItemAlergenoService', () => {
  let service: ItemAlergenoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ItemAlergenoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
