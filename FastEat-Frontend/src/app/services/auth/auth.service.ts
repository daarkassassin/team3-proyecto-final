import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/internal/Observable";
import { map } from "rxjs/operators";
import { usuario } from "src/app/models/usuario";
import { TokenStorage } from 'src/app/services/auth/token-storage.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private tokenStorage: TokenStorage) {

  }

  registerUser(username: String, email: String, password: String): Observable<any> {
    return this.http
      .post<usuario>(
        GlobalConstantsService.authURL + 'register',
        { username, email, password}, {headers: new HttpHeaders({"Content-Type": "application/json"}), observe: 'response'}
      )
      .pipe(map(data => data));
  }

  loginuser(username: String, password: String){
    return this.http
    .post<usuario>(
      GlobalConstantsService.authURL + 'login',{username, password}, {headers: new HttpHeaders({"Content-Type": "application/json"}), observe: 'response'}
    )
    .pipe(map(data => data));
  }

  getCurrentUser(username: String): Observable<usuario>{
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    return this.http.get<usuario>(GlobalConstantsService.authURL + 'api/usuario/username/' + username, {headers});
  }

  logoutUser() {

    //const url_api = `http://localhost/api/logout?access_token=${accessToken}`;
    //return this.htttp.post<usuario>(url_api, { headers: this.headers });
  }

}
