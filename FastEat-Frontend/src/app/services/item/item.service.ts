import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from '../auth/token-storage.service';
import { item } from 'src/app/models/item';
import { ItemAlergenoService } from '../item_Alergeno/item-alergeno.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(private tokenStorage: TokenStorage, private http: HttpClient, private itemAlergenoService: ItemAlergenoService) { }

  getArticulosList(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    return this.http.get<any>(GlobalConstantsService.apiURL + '/items', {headers})
  }

  getArticulo(id: Number): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    return this.http.get<any>(GlobalConstantsService.apiURL + '/item/' + id, {headers})
  }

  createArticulo(articulo: item, selectedAlergenos: any){
    let datareturn: any;
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders({"Authorization": token, "Content-Type": "application/json"})
    this.http.post<item>(GlobalConstantsService.apiURL + "/item/", articulo, {headers}).subscribe(data => {
      this.itemAlergenoService.crearArticulosAlergenos(data, selectedAlergenos);
    });


  }
  updateArticulo(articulo: item, selectedAlergenos: any){
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders({"Authorization": token, "Content-Type": "application/json"})
    this.itemAlergenoService.deleteArticulosAlergenosByItemID(articulo.id_item);
    this.http.put<item>(GlobalConstantsService.apiURL + "/item/" + articulo.id_item, articulo, {headers}).subscribe(data => {
      this.itemAlergenoService.crearArticulosAlergenos(data, selectedAlergenos);
    });
  }

  deleteArticulo(id: number, articulo: item) {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders({"Authorization": token, "Content-Type": "application/json"})
    this.http.put<item>(GlobalConstantsService.apiURL + "/item/" + id, articulo, {headers}).subscribe(data => data);
  }



}
