import { TestBed } from '@angular/core/testing';

import { FranjaHorariaService } from './franja-horaria.service';

describe('FranjaHorariaService', () => {
  let service: FranjaHorariaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FranjaHorariaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
