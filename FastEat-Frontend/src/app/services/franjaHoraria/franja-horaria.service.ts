import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { franjaHoraria } from 'src/app/models/franjaHoraria';
import { TokenStorage } from '../auth/token-storage.service';
import{ GlobalConstantsService } from 'src/app/services/global-constants.service';

@Injectable({
  providedIn: 'root'
})
export class FranjaHorariaService {

  franjaH: franjaHoraria = {
    id_franjahoraria: 0,
    horaInicio: '',
    horaFinal: '',
    ocupacionMaxima: 0,
    ocupacionActual: 0
  }

  constructor(private tokenStorage: TokenStorage, private http: HttpClient) { }

  getFranjasHorarias(): Observable<any> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<any>(GlobalConstantsService.apiURL + '/franjasHorariasDisponibles', {headers});
  }

  getFranjaHorariaPorId(id: number): Observable<franjaHoraria> {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);

    return this.http.get<franjaHoraria>(GlobalConstantsService.apiURL + '/franjaHoraria/' + id, {headers});
  }

  modificarFranjaHoraria(fh: franjaHoraria, id: number) {
    let token = this.tokenStorage.getToken();
    let headers = new HttpHeaders().set('Authorization', token);
    console.log(fh);
    this.http.put<any>(GlobalConstantsService.apiURL + '/franjaHoraria/' + id, fh, {headers}).subscribe(data => console.log(data));
  }

}
