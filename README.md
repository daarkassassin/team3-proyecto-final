#
![banerGit](https://gitlab.com/daarkassassin/team3-proyecto-final/-/raw/dev1/banner.png)
# Proyecto - Fast Eat

#### 1. Equipo Desarrollo 

| Developer | Rama | Rol | Fecha Incorporación |  Versión |
| ---       | :---:|:---:| :---:               | :---: |
| David B. | Master | Project Manager | 07/03/2021 | alpha |
| Xavi B. | Master | Project Manager | 07/03/2021 | alpha |

#### 2. Description
```
Aplicativo para gestionar los usuarios y menus del comedor.

```
#### 3. Link a un demo con el proyecto desplegado: https://github.com/
```
* Nombre de la App: [GITTT] (https://github.com/)
```
#### 4. Lista con los pasos mínimos que se necesitan para clonar exitosamente el proyecto y echarlo a andar en local.

###### Install

> + Backend
> 	+ [Java - jdk-8](https://www.oracle.com/es/java/technologies/javase/javase-jdk8-downloads.html)
> 	+ [Apache Maven](http://maven.apache.org/download.cgi)
> 	+ [IDE - Spring Tool Suite 4](https://spring.io/tools)

> + FrontEnd
> 	+ [Visual Studio Code](https://code.visualstudio.com/)
> 		+  [Angular Essentials (Version 11)](https://marketplace.visualstudio.com/items?itemName=johnpapa.angular-essentials)
> 		+  [Bootstrap5](https://marketplace.visualstudio.com/items?itemName=ajanuw.bs5)
> 		+  [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
> 		+  [vscode-icons](https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons)
> 		+  [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script)
> 		+  [Debugger for Firefox](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug)
> 	+ [node: 14.16.0 LTS](https://nodejs.org/en/)
> 	+ npm: 6.14.11  
> 		+ `npm install npm@latest -g`
> 	+ Angular CLI: 11.2.5 - 
> 		+ `npm install -g @angular/cli`

###### Command line 
```

```

#### 5. Screenshot.
![Imagen](https://gitlab.com/daarkassassin/team3-proyecto-final/-/raw/dev2/front_1.png)
    
----

### Lista de contenidos
+ Directorios
	+ plantilla 	`[ Plantilla del proyecto ]`
	+ postman 		`[ Export de todos los end points de nuestra API ]`
	+ sql			`[ Archivos .sql Necesarios para que el proyecto funcione ]`
	+ Backend		`[ src de la API ]`
	+ FrontEnd		`[ src de la web ]`

----
### End
