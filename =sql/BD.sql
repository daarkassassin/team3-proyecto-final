CREATE DATABASE IF NOT EXISTS `ProyectoRestaurante`;
USE `ProyectoRestaurante`;

-- Drops
DROP TABLE IF EXISTS `rol_usuario`;
DROP TABLE IF EXISTS `usuario_alergenos`;
DROP TABLE IF EXISTS `roles`;
DROP TABLE IF EXISTS `pedidos_item`;
DROP TABLE IF EXISTS `pedidos`;
DROP TABLE IF EXISTS `franjahoraria`;
DROP TABLE IF EXISTS `usuario`;
DROP TABLE IF EXISTS `item_alergenos`;
DROP TABLE IF EXISTS `alergenos`;
DROP TABLE IF EXISTS `item`;
DROP TABLE IF EXISTS `franjahoraria`;
DROP TABLE IF EXISTS `categoria`;

-- roles(id_rol, nombre);
CREATE TABLE `roles` (
  `id_rol` int NOT NULL auto_increment,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
);

insert into `roles`(nombre) values ('Administrador');
insert into `roles`(nombre) values ('usuario');

-- usuario(id_usuario, username, password, email, nombre, apellidos);
CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL auto_increment,
  `username` varchar(250),
  `password` varchar(250) DEFAULT NULL,
  `email` varchar(250),
  `nombre` varchar(250) DEFAULT NULL,
  `apellidos` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
);

insert into `usuario`(username, password, email, nombre, apellidos) values ('admin','$2a$10$EYZh7Tp4WCk62WtB5B8ydOY88Z1pKAS9kjtJetS6xTyREUPdEtL4S','admin@gmail.com','admin','admin');
insert into `usuario`(username, password, email, nombre, apellidos) values ('xavi','$2a$10$EYZh7Tp4WCk62WtB5B8ydOY88Z1pKAS9kjtJetS6xTyREUPdEtL4S','xavi@gmail.com','Xavi','Bonet');
insert into `usuario`(username, password, email, nombre, apellidos) values ('david','$2a$10$EYZh7Tp4WCk62WtB5B8ydOY88Z1pKAS9kjtJetS6xTyREUPdEtL4S','david@gmail.com','David','Bonet');
insert into `usuario`(username, password, email, nombre, apellidos) values ('jose','$2a$10$EYZh7Tp4WCk62WtB5B8ydOY88Z1pKAS9kjtJetS6xTyREUPdEtL4S','jose@gmail.com','Jose','lorem');
insert into `usuario`(username, password, email, nombre, apellidos) values ('alba','$2a$10$EYZh7Tp4WCk62WtB5B8ydOY88Z1pKAS9kjtJetS6xTyREUPdEtL4S','alba@gmail.com','Alba','ipsum');
-- rol_usuario(id_rol, id_usuario);
CREATE TABLE `rol_usuario` (
  `id` int NOT NULL auto_increment,
  `id_usuario` int NOT NULL,
  `id_rol` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `rol_usuario_fk1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `rol_usuario_fk2` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`)
);

insert into `rol_usuario`(id_usuario, id_rol) values (1,1);
insert into `rol_usuario`(id_usuario, id_rol) values (2,2);
insert into `rol_usuario`(id_usuario, id_rol) values (3,2);
insert into `rol_usuario`(id_usuario, id_rol) values (4,2);
insert into `rol_usuario`(id_usuario, id_rol) values (5,2);

-- alergenos(id_alergeno, nombre);
CREATE TABLE `alergenos` (
  `id_alergeno` int NOT NULL auto_increment,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_alergeno`)
);

insert into `alergenos`(nombre) values ('altramuces');
insert into `alergenos`(nombre) values ('apio');
insert into `alergenos`(nombre) values ('cacahuetes');
insert into `alergenos`(nombre) values ('crustáceos');
insert into `alergenos`(nombre) values ('dióxido');
insert into `alergenos`(nombre) values ('frutos');
insert into `alergenos`(nombre) values ('gluten');
insert into `alergenos`(nombre) values ('granos');
insert into `alergenos`(nombre) values ('huevos');
insert into `alergenos`(nombre) values ('lacteos');
insert into `alergenos`(nombre) values ('moluscos');
insert into `alergenos`(nombre) values ('mostaza');
insert into `alergenos`(nombre) values ('pescado');
insert into `alergenos`(nombre) values ('soja');

-- usuario_alergenos(id_usuario, id_alergeno);
CREATE TABLE `usuario_alergenos` (
  `id` int NOT NULL auto_increment,
  `id_usuario` int NOT NULL,
  `id_alergeno` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `usuario_alergenos_fk1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `usuario_alergenos_fk2` FOREIGN KEY (`id_alergeno`) REFERENCES `alergenos` (`id_alergeno`)
);

insert into `usuario_alergenos`(id_usuario, id_alergeno) values (1,1);
insert into `usuario_alergenos`(id_usuario, id_alergeno) values (2,1);
insert into `usuario_alergenos`(id_usuario, id_alergeno) values (3,2);
insert into `usuario_alergenos`(id_usuario, id_alergeno) values (4,3);
insert into `usuario_alergenos`(id_usuario, id_alergeno) values (5,4);

-- franjahoraria(id_franjahoraria, horaInicio, horaFinal, ocupacionMaxima);
CREATE TABLE `franjahoraria` (
  `id_franjahoraria` int NOT NULL auto_increment,
  `hora_inicio` varchar(250) NOT NULL,
  `hora_final` varchar(250) NOT NULL,
  `ocupacion_maxima` int NOT NULL,
  `ocupacion_actual` int NOT NULL,
  PRIMARY KEY (`id_franjahoraria`)
);

insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('12:00:00', '12:15:00', 10, 5);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('12:15:00', '12:30:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('12:30:00', '12:45:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('12:45:00', '13:00:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('13:00:00', '13:15:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('13:15:00', '13:30:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('13:30:00', '13:45:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('13:45:00', '14:00:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('14:00:00', '14:15:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('14:15:00', '14:30:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('14:30:00', '14:45:00', 10, 0);
insert into `franjahoraria`(hora_inicio, hora_final, ocupacion_maxima ,ocupacion_actual) values ('14:45:00', '15:00:00', 10, 0);

-- pedidos(id_pedido, fecha, precioTotal, Iva,  id_usuario, id_franjahoraria);
CREATE TABLE `pedidos` (
  `id_pedido` int NOT NULL auto_increment,
  `fecha` date NOT NULL,
  `precio_total` double,
  `iva` double,
  `id_usuario` int NOT NULL,
  `id_franjahoraria` int NOT NULL,
  PRIMARY KEY (`id_pedido`),
  CONSTRAINT `usuario_fk1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  CONSTRAINT `franjahoraria_fk1` FOREIGN KEY (`id_franjahoraria`) REFERENCES `franjahoraria` (`id_franjahoraria`)
);

insert into `pedidos`(fecha, precio_total, iva, id_usuario, id_franjahoraria) values ('2021-10-05 10:05:00', 36.99, 21, 2, 1);
insert into `pedidos`(fecha, precio_total, iva, id_usuario, id_franjahoraria) values ('2021-10-07 10:12:00', 40.99, 21, 2, 1);
insert into `pedidos`(fecha, precio_total, iva, id_usuario, id_franjahoraria) values ('2021-10-09 10:10:00', 15.99, 21, 2, 1);
insert into `pedidos`(fecha, precio_total, iva, id_usuario, id_franjahoraria) values ('2021-10-11 10:07:00', 25.99, 21, 2, 1);
insert into `pedidos`(fecha, precio_total, iva, id_usuario, id_franjahoraria) values ('2021-10-15 10:09:00', 19.99, 21, 2, 1);

-- categoria(id_categoria, nombre, descripcion);
CREATE TABLE `categoria` (
  `id_categoria` int NOT NULL auto_increment,
  `nombre` varchar(250),
  `descripcion` varchar(250),
  PRIMARY KEY (`id_categoria`)
);

insert into `categoria`(nombre, descripcion) values ('Primer plato', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum');
insert into `categoria`(nombre, descripcion) values ('Segundo plato', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum');
insert into `categoria`(nombre, descripcion) values ('Postres', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum');
insert into `categoria`(nombre, descripcion) values ('Bebidas', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum');
insert into `categoria`(nombre, descripcion) values ('Lorem ipsum', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum');

-- item(id_item, nombre, imagen, descripcion, precio, id_categoria);
CREATE TABLE `item` (
  `id_item` int NOT NULL auto_increment,
  `nombre` varchar(250),
  `descripcion` varchar(250),
  `imagen` LONGTEXT, 
  `precio` double,
  `status` tinyint(1),
  `id_categoria` int NOT NULL,
  PRIMARY KEY (`id_item`),
  CONSTRAINT `item_fk1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`)
);

insert into `item`(nombre, descripcion, precio, id_categoria, status) values ('Pan', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 0.6, 1 ,1);
insert into `item`(nombre, descripcion, precio, id_categoria, status) values ('Lorem ipsum', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 6, 2 ,1);
insert into `item`(nombre, descripcion, precio, id_categoria, status) values ('Lorem ipsum', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 7.5, 3 ,1);
insert into `item`(nombre, descripcion, precio, id_categoria, status) values ('Lorem ipsum', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 5, 3 ,1);
insert into `item`(nombre, descripcion, precio, id_categoria, status) values ('Lorem ipsum', 'Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum', 2, 4 ,1);

-- Pedido_item(id_pedido, id_item);
CREATE TABLE `pedidos_item` (
  `id` int NOT NULL auto_increment,
  `id_pedido` int NOT NULL,
  `id_item` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `pedidos_item_fk1` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id_pedido`),
  CONSTRAINT `pedidos_item_fk2` FOREIGN KEY (`id_item`) REFERENCES `item` (`id_item`)
);

insert into `pedidos_item`(id_pedido, id_item) values (1,1);
insert into `pedidos_item`(id_pedido, id_item) values (2,2);
insert into `pedidos_item`(id_pedido, id_item) values (3,3);
insert into `pedidos_item`(id_pedido, id_item) values (2,1);
insert into `pedidos_item`(id_pedido, id_item) values (3,2);

-- item_alergenos(id_item, id_alergeno);
CREATE TABLE `item_alergenos` (
  `id` int NOT NULL auto_increment,
  `id_item` int NOT NULL,
  `id_alergeno` int NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `item_alergenos_fk1` FOREIGN KEY (`id_item`) REFERENCES `item` (`id_item`),
  CONSTRAINT `item_alergenos_fk2` FOREIGN KEY (`id_alergeno`) REFERENCES `alergenos` (`id_alergeno`)
);

insert into `item_alergenos`(id_item, id_alergeno) values (1,1);
insert into `item_alergenos`(id_item, id_alergeno) values (2,2);
insert into `item_alergenos`(id_item, id_alergeno) values (3,3);
insert into `item_alergenos`(id_item, id_alergeno) values (2,1);
insert into `item_alergenos`(id_item, id_alergeno) values (3,2);
