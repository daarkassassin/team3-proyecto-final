package equipo3.proyecto.FastEatAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastEatApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FastEatApiApplication.class, args);
	}

}
