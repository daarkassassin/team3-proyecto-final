package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "item")
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_item")
	private Long id_item;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "imagen")
	private String imagen;
	
	@Column(name = "precio")
	private double precio;
	
	@Column(name = "status")
	private boolean status;

	@ManyToOne
	@JoinColumn(name = "id_categoria")
	Categoria Categoria;

	@OneToMany
	@JoinColumn(name = "id_item")
	private List<Item_Alergeno> Item_Alergeno;

	public Item() {

	}

	/**
	 * @param id_item
	 * @param nombre
	 * @param descripcion
	 * @param imagen
	 * @param categoria
	 */
	public Item(Long id_item, String nombre, String descripcion, String imagen, double precio, boolean status, Categoria categoria) {
		this.id_item = id_item;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.precio = precio;
		this.status = status;
		Categoria = categoria;
	}

	/**
	 * @return the id_item
	 */
	public Long getId_item() {
		return id_item;
	}

	/**
	 * @param id_item the id_item to set
	 */
	public void setId_item(Long id_item) {
		this.id_item = id_item;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the imagen
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * @param imagen the imagen to set
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * @return the categoria
	 */
	public Categoria getCategoria() {
		return Categoria;
	}

	/**
	 * @param categoria the categoria to set
	 */
	public void setCategoria(Categoria categoria) {
		Categoria = categoria;
	}

	/**
	 * @return the item_Alergeno
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pedidos_item")
	public List<Item_Alergeno> getItem_Alergeno() {
		return Item_Alergeno;
	}

	/**
	 * @param item_Alergeno the item_Alergeno to set
	 */
	public void setItem_Alergeno(List<Item_Alergeno> item_Alergeno) {
		Item_Alergeno = item_Alergeno;
	}	
}
