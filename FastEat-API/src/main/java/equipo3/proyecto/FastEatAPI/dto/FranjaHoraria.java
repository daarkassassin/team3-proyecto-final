package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "franjahoraria")
public class FranjaHoraria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_franjahoraria")
	private Long id_franjahoraria;

	
	@Column(name = "horaInicio")
	private String horaInicio;

	@Column(name = "horaFinal")
	private String horaFinal;

	@Column(name = "ocupacionMaxima")
	private int ocupacionMaxima;
	
	@Column(name = "ocupacionActual")
	private int ocupacionActual;

	@OneToMany
	@JoinColumn(name = "id_franjahoraria")
	private List<Pedido> Pedido;

	public FranjaHoraria() {

	}

	/**
	 * @param id_franjahoraria
	 * @param horaInicio
	 * @param horaFinal
	 * @param ocupacionMaxima
	 * @param ocupacionActual
	 * @param pedidos
	 */
	public FranjaHoraria(Long id_franjahoraria, String horaInicio, String horaFinal, int ocupacionMaxima, int ocupacionActual) {
		
		this.id_franjahoraria = id_franjahoraria;
		this.horaInicio = horaInicio;
		this.horaFinal = horaFinal;
		this.ocupacionMaxima = ocupacionMaxima;
		this.ocupacionActual = ocupacionActual;
	}

	/**
	 * @return the id_franjahoraria
	 */
	public Long getId_franjahoraria() {
		return id_franjahoraria;
	}

	/**
	 * @param id_franjahoraria the id_franjahoraria to set
	 */
	public void setId_franjahoraria(Long id_franjahoraria) {
		this.id_franjahoraria = id_franjahoraria;
	}

	/**
	 * @return the horaInicio
	 */
	public String getHoraInicio() {
		return horaInicio;
	}

	/**
	 * @param horaInicio the horaInicio to set
	 */
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * @return the horaFinal
	 */
	public String getHoraFinal() {
		return horaFinal;
	}

	/**
	 * @param horaFinal the horaFinal to set
	 */
	public void setHoraFinal(String horaFinal) {
		this.horaFinal = horaFinal;
	}

	/**
	 * @return the ocupacionMaxima
	 */
	public int getOcupacionMaxima() {
		return ocupacionMaxima;
	}

	/**
	 * @param ocupacionMaxima the ocupacionMaxima to set
	 */
	public void setOcupacionMaxima(int ocupacionMaxima) {
		this.ocupacionMaxima = ocupacionMaxima;
	}
	
	public int getOcupacionActual() {
		return ocupacionActual;
	}

	public void setOcupacionActual(int ocupacionActual) {
		this.ocupacionActual = ocupacionActual;
	}

	/**
	 * @return the pedidos
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pedidos")
	public List<Pedido> getPedidos() {
		return Pedido;
	}

	/**
	 * @param pedido the pedidos to set
	 */
	public void setPedidos(List<Pedido> pedido) {
		Pedido = pedido;
	}
	
}
