package equipo3.proyecto.FastEatAPI.dto;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pedidos")
public class Pedido {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pedido")
	private Long id_pedido;

	@Column(name = "fecha")
	private Date fecha;

	@Column(name = "precioTotal")
	private double precioTotal;

	@Column(name = "iva")
	private int iva;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	Usuario Usuario;

	@ManyToOne
	@JoinColumn(name = "id_franjahoraria")
	FranjaHoraria FranjaHoraria;
	
	@OneToMany
	@JoinColumn(name = "id_pedido")
	private List<Pedido_Item> Pedido_Item;

	public Pedido() {

	}

	/**
	 * @param id_pedido
	 * @param fecha
	 * @param precioTotal
	 * @param iva
	 * @param usuario
	 * @param franjaHoraria
	 */
	public Pedido(Long id_pedido, Date fecha, double precioTotal, int iva, Usuario usuario, FranjaHoraria franjaHoraria) {
		this.id_pedido = id_pedido;
		this.fecha = fecha;
		this.precioTotal = precioTotal;
		this.iva = iva;
		Usuario = usuario;
		FranjaHoraria = franjaHoraria;
	}

	/**
	 * @return the id_pedido
	 */
	public Long getId_pedido() {
		return id_pedido;
	}

	/**
	 * @param id_pedido the id_pedido to set
	 */
	public void setId_pedido(Long id_pedido) {
		this.id_pedido = id_pedido;
	}

	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the precioTotal
	 */
	public double getPrecioTotal() {
		return precioTotal;
	}

	/**
	 * @param precioTotal the precioTotal to set
	 */
	public void setPrecioTotal(double precioTotal) {
		this.precioTotal = precioTotal;
	}

	/**
	 * @return the iva
	 */
	public int getIva() {
		return iva;
	}

	/**
	 * @param iva the iva to set
	 */
	public void setIva(int iva) {
		this.iva = iva;
	}

	/**
	 * @return the usuario
	 */
	public Usuario getUsuario() {
		return Usuario;
	}

	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Usuario usuario) {
		Usuario = usuario;
	}

	/**
	 * @return the franjaHoraria
	 */
	public FranjaHoraria getFranjaHoraria() {
		return FranjaHoraria;
	}

	/**
	 * @param franjaHoraria the franjaHoraria to set
	 */
	public void setFranjaHoraria(FranjaHoraria franjaHoraria) {
		FranjaHoraria = franjaHoraria;
	}

	/**
	 * @return the pedido_Item
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pedidos_item")
	public List<Pedido_Item> getPedido_Item() {
		return Pedido_Item;
	}

	/**
	 * @param pedido_Item the pedido_Item to set
	 */
	public void setPedido_Item(List<Pedido_Item> pedido_Item) {
		Pedido_Item = pedido_Item;
	}
	
	

}
