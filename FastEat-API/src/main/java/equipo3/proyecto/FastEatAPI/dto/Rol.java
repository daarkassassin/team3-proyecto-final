package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "roles")
public class Rol {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rol")
	private Long id_rol;

	@Column(name = "nombre")
	private String nombre;

	@OneToMany
	@JoinColumn(name = "id_rol")
	private List<Rol_usuario> Rol_usuario;

	// Constructores
	public Rol() {
	}

	public Rol(Long id_rol, String nombre) {
		this.id_rol = id_rol;
		this.nombre = nombre;
	}

	// Metodos SET y GET

	/**
	 * @return the id_rol
	 */
	public Long getId_rol() {
		return id_rol;
	}

	/**
	 * @param id_rol the id_rol to set
	 */
	public void setId_rol(Long id_rol) {
		this.id_rol = id_rol;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the rol_usuario
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rol_usuario")
	public List<Rol_usuario> getRol_usuario() {
		return Rol_usuario;
	}

	/**
	 * @param rol_usuario the rol_usuario to set
	 */
	public void setRol_usuario(List<Rol_usuario> rol_usuario) {
		Rol_usuario = rol_usuario;
	}

}
