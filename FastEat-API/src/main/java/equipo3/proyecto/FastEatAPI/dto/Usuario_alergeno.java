package equipo3.proyecto.FastEatAPI.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "usuario_alergenos")
public class Usuario_alergeno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_usuario")
	Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "id_alergeno")
	Alergeno alergeno;

	public Usuario_alergeno() {

	}

	public Usuario_alergeno(Long id, Usuario usuario, Alergeno alergeno) {
		this.id = id;
		this.usuario = usuario;
		this.alergeno = alergeno;
	}

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Alergeno getAlergeno() {
		return alergeno;
	}

	public void setAlergeno(Alergeno alergeno) {
		this.alergeno = alergeno;
	}

}
