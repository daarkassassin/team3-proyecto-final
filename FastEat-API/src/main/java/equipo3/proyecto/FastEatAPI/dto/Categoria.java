package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "categoria")
public class Categoria {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categoria")
	private Long id_categoria;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "descripcion")
	private String descripcion;
	
	@OneToMany
	@JoinColumn(name = "id_categoria")
	private List<Item> Item;

	public Categoria() {
		
	}
	
	/**
	 * @param id_categoria
	 * @param nombre
	 * @param descripcion
	 */
	public Categoria(Long id_categoria, String nombre, String descripcion) {
		this.id_categoria = id_categoria;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
	 * @return the id_categoria
	 */
	public Long getId_categoria() {
		return id_categoria;
	}

	/**
	 * @param id_categoria the id_categoria to set
	 */
	public void setId_categoria(Long id_categoria) {
		this.id_categoria = id_categoria;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the item
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
	public List<Item> getItem() {
		return Item;
	}

	/**
	 * @param item the item to set
	 */
	public void setItem(List<Item> item) {
		Item = item;
	}
	
	
}
