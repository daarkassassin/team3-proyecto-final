package equipo3.proyecto.FastEatAPI.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item_alergenos")
public class Item_Alergeno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "id_item")
	Item item;

	@ManyToOne
	@JoinColumn(name = "id_alergeno")
	Alergeno alergeno;

	public Item_Alergeno() {

	}

	public Item_Alergeno(Long id, Item item, Alergeno alergeno) {
		this.id = id;
		this.item = item;
		this.alergeno = alergeno;
	}

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Alergeno getAlergeno() {
		return alergeno;
	}

	public void setAlergeno(Alergeno alergeno) {
		this.alergeno = alergeno;
	}

}
