package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private Long id_usuario;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@Column(name = "email")
	private String email;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellidos")
	private String apellidos;

	@OneToMany
	@JoinColumn(name = "id_usuario")
	private List<Rol_usuario> Rol_usuario;

	@OneToMany
	@JoinColumn(name = "id_usuario")
	private List<Pedido> Pedido;

	public Usuario() {

	}

	public Usuario(Long id_usuario, String username, String password, String email, String nombre, String apellidos) {
		this.id_usuario = id_usuario;
		this.username = username;
		this.password = password;
		this.email = email;
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	public Long getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Long id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "rol_usuario")
	public List<Rol_usuario> getRol_usuario() {
		return Rol_usuario;
	}

	public void setRol_usuario(List<Rol_usuario> rol_usuario) {
		Rol_usuario = rol_usuario;
	}

	/**
	 * @return the pedidos
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pedidos")
	public List<Pedido> getPedidos() {
		return Pedido;
	}

	/**
	 * @param pedido the pedidos to set
	 */
	public void setPedidos(List<Pedido> pedido) {
		Pedido = pedido;
	}

}
