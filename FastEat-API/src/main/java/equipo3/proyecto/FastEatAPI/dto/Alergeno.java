package equipo3.proyecto.FastEatAPI.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "alergenos")
public class Alergeno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_alergeno")
	private Long id_alergeno;

	@Column(name = "nombre")
	private String nombre;

	@OneToMany
	@JoinColumn(name = "id_alergeno")
	private List<Usuario_alergeno> usuario_alergeno;
	
	@OneToMany
	@JoinColumn(name = "id_alergeno")
	private List<Item_Alergeno> item_alergeno;

	public Alergeno() {

	}

	public Alergeno(Long id_alergeno, String nombre, List<Usuario_alergeno> usuario_alergeno) {
		this.id_alergeno = id_alergeno;
		this.nombre = nombre;
		this.usuario_alergeno = usuario_alergeno;
	}

	public Long getId_alergeno() {
		return id_alergeno;
	}

	public void setId_alergeno(Long id_alergeno) {
		this.id_alergeno = id_alergeno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "alergenos")
	public List<Usuario_alergeno> getUsuario_alergeno() {
		return usuario_alergeno;
	}

	public void setUsuario_alergeno(List<Usuario_alergeno> usuario_alergeno) {
		this.usuario_alergeno = usuario_alergeno;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "item")
	public List<Item_Alergeno> getItem_alergeno() {
		return item_alergeno;
	}

	public void setItem_alergeno(List<Item_Alergeno> item_alergeno) {
		this.item_alergeno = item_alergeno;
	}
	
	

}
