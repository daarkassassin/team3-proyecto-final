package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Pedido;

public interface IPedidoDAO extends JpaRepository<Pedido, Long>{

}
