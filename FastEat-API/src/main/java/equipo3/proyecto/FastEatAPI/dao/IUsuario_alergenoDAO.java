package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Usuario_alergeno;

public interface IUsuario_alergenoDAO extends JpaRepository<Usuario_alergeno, Long>{

}
