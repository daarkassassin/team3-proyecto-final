package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Alergeno;

public interface IAlergenoDAO extends JpaRepository<Alergeno, Long>{

}
