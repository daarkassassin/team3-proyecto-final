package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Rol_usuario;

public interface IRol_usuarioDAO extends JpaRepository<Rol_usuario, Long> {

}
