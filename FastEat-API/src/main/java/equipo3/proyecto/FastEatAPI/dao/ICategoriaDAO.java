package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Categoria;

public interface ICategoriaDAO extends JpaRepository<Categoria, Long>{

}
