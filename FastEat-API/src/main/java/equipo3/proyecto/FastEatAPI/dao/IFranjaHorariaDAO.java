package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.FranjaHoraria;

public interface IFranjaHorariaDAO extends JpaRepository<FranjaHoraria, Long>{

}
