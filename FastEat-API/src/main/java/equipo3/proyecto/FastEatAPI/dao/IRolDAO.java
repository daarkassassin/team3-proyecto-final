package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Rol;

public interface IRolDAO extends JpaRepository<Rol, Long>{

}
