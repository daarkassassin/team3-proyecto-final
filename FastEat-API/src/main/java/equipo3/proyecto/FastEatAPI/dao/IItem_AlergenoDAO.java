package equipo3.proyecto.FastEatAPI.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.dto.Item_Alergeno;

public interface IItem_AlergenoDAO extends JpaRepository<Item_Alergeno, Long>{
	
	public Item deleteByItem(Item item);
	public List<Item_Alergeno> findByItem(Item item);

}
