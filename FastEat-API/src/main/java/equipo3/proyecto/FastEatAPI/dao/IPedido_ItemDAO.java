package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Pedido_Item;

public interface IPedido_ItemDAO extends JpaRepository<Pedido_Item, Long>{

}
