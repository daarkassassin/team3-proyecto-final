package equipo3.proyecto.FastEatAPI.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import equipo3.proyecto.FastEatAPI.dto.Item;

public interface IItemDAO extends JpaRepository<Item, Long>{
	

}
