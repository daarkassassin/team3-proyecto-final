package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.dto.Item_Alergeno;
import equipo3.proyecto.FastEatAPI.service.Item_AlergenoServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class Item_AlergenoController{
	
	@Autowired
	Item_AlergenoServiceImpl item_AlergenoServiceImpl;
	
	@GetMapping("/items_Alergenos")
	public List<Item_Alergeno> listarItem_Alergeno(){
		return item_AlergenoServiceImpl.listarItem_Alergeno();
	}
	
	@PostMapping("/item_Alergeno")
	public Item_Alergeno salvarItem_Alergeno(@RequestBody Item_Alergeno item_Alergeno) {
		return item_AlergenoServiceImpl.guardarItem_Alergeno(item_Alergeno);
	}
		
	@GetMapping("/item_Alergeno/{id}")
	public Item_Alergeno item_AlergenosXID(@PathVariable(name="id") Long id) {
		
		Item_Alergeno item_Alergenos_xid= new Item_Alergeno();
		
		item_Alergenos_xid = item_AlergenoServiceImpl.item_AlergenoXID(id);
		
		System.out.println("Item_Alergeno XID: "+item_Alergenos_xid);
		
		return item_Alergenos_xid;
	}
	
	@PutMapping("/item_Alergeno/{id}")
	public Item_Alergeno actualizarItem_Alergeno(@PathVariable(name="id")Long id,@RequestBody Item_Alergeno item_Alergeno) {
		
		Item_Alergeno item_Alergeno_seleccionado= new Item_Alergeno();
		Item_Alergeno item_Alergeno_actualizado= new Item_Alergeno();
		
		item_Alergeno_seleccionado= item_AlergenoServiceImpl.item_AlergenoXID(id);
		
		item_Alergeno_seleccionado.setAlergeno(item_Alergeno.getAlergeno());
		item_Alergeno_seleccionado.setItem(item_Alergeno.getItem());
		
		item_Alergeno_actualizado = item_AlergenoServiceImpl.actualizarItem_Alergeno(item_Alergeno_seleccionado);
		
		System.out.println("El Item_Alergeno actualizado es: "+ item_Alergeno_actualizado);
		
		return item_Alergeno_actualizado;
	}
	
	
	@DeleteMapping("/item_Alergeno/{id}")
	public void eleiminarItem_Alergeno(@PathVariable(name="id")Long id) {
		item_AlergenoServiceImpl.eliminarItem_Alergeno(id);
	}
	
	@GetMapping("/item_Alergeno/item/")
	public List<Item_Alergeno> item_AlergenosXItem(@RequestBody Item item) {
		return item_AlergenoServiceImpl.item_AlergenoXItem(item);
	}
	
	@DeleteMapping("/item_Alergeno/item/delete/{id}")
	public void item_AlergenosXItemDelete(@PathVariable(name="id")Long id) {
		Item item = new Item();
		item.setId_item(id);
		
		List<Item_Alergeno> lista = item_AlergenoServiceImpl.item_AlergenoXItem(item);
		lista.forEach((temp)->{
			item_AlergenoServiceImpl.eliminarItem_Alergeno(temp.getId());
		});
		
	}
	
}