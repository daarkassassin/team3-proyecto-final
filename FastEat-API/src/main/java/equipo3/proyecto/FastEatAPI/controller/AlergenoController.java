package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Alergeno;
import equipo3.proyecto.FastEatAPI.service.AlergenoServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class AlergenoController{
	
	@Autowired
	AlergenoServiceImpl alergenoServiceImpl;
	
	@GetMapping("/alergenos")
	public List<Alergeno> listarAlergeno(){
		return alergenoServiceImpl.listarAlergeno();
	}
	
	@PostMapping("/alergeno")
	public Alergeno salvarAlergeno(@RequestBody Alergeno alergeno) {
		return alergenoServiceImpl.guardarAlergeno(alergeno);
	}
		
	@GetMapping("/alergeno/{id}")
	public Alergeno alergenosXID(@PathVariable(name="id") Long id) {
		
		Alergeno alergenos_xid= new Alergeno();
		
		alergenos_xid = alergenoServiceImpl.alergenoXID(id);
		
		System.out.println("Alergeno XID: "+alergenos_xid);
		
		return alergenos_xid;
	}
	
	@PutMapping("/alergeno/{id}")
	public Alergeno actualizarAlergeno(@PathVariable(name="id")Long id,@RequestBody Alergeno alergeno) {
		
		Alergeno alergeno_seleccionado= new Alergeno();
		Alergeno alergeno_actualizado= new Alergeno();
		
		alergeno_seleccionado= alergenoServiceImpl.alergenoXID(id);
		
		alergeno_seleccionado.setNombre(alergeno.getNombre());
		
		alergeno_actualizado = alergenoServiceImpl.actualizarAlergeno(alergeno_seleccionado);
		
		System.out.println("El Alergeno actualizado es: "+ alergeno_actualizado);
		
		return alergeno_actualizado;
	}
	
	@DeleteMapping("/alergeno/{id}")
	public void eleiminarAlergeno(@PathVariable(name="id")Long id) {
		alergenoServiceImpl.eliminarAlergeno(id);
	}
	
}