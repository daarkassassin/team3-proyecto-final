package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.service.ItemServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class ItemController{
	
	@Autowired
	ItemServiceImpl itemServiceImpl;
	
	@GetMapping("/items")
	public List<Item> listarItem(){
		return itemServiceImpl.listarItem();
	}
	
	@PostMapping("/item")
	public Item salvarItem(@RequestBody Item item) {
		return itemServiceImpl.guardarItem(item);
	}
		
	@GetMapping("/item/{id}")
	public Item itemsXID(@PathVariable(name="id") Long id) {
		
		Item items_xid= new Item();
		
		items_xid = itemServiceImpl.itemXID(id);
		
		System.out.println("Item XID: "+items_xid);
		
		return items_xid;
	}	
	
	@PutMapping("/item/{id}")
	public Item actualizarItem(@PathVariable(name="id")Long id,@RequestBody Item item) {
		
		Item item_seleccionado= new Item();
		Item item_actualizado= new Item();
		
		item_seleccionado= itemServiceImpl.itemXID(id);
		 
		item_seleccionado.setNombre(item.getNombre());
		item_seleccionado.setDescripcion(item.getDescripcion());
		item_seleccionado.setImagen(item.getImagen());
		item_seleccionado.setPrecio(item.getPrecio());
		item_seleccionado.setStatus(item.isStatus()); 
		item_seleccionado.setCategoria(item.getCategoria());
		
		item_actualizado = itemServiceImpl.actualizarItem(item_seleccionado);
		
		System.out.println("El Item actualizado es: "+ item_actualizado);
		
		return item_actualizado;  
	}
	
	@DeleteMapping("/item/{id}")
	public void eleiminarItem(@PathVariable(name="id")Long id) {
		itemServiceImpl.eliminarItem(id);
	}
	
}