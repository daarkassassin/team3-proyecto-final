package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Categoria;
import equipo3.proyecto.FastEatAPI.service.CategoriaServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class CategoriaController{
	
	@Autowired
	CategoriaServiceImpl categoriaServiceImpl;
	
	@GetMapping("/categorias")
	public List<Categoria> listarCategoria(){
		return categoriaServiceImpl.listarCategoria();
	}
	
	@PostMapping("/categoria")
	public Categoria salvarCategoria(@RequestBody Categoria categoria) {
		return categoriaServiceImpl.guardarCategoria(categoria);
	}
		
	@GetMapping("/categoria/{id}")
	public Categoria categoriasXID(@PathVariable(name="id") Long id) {
		
		Categoria categorias_xid= new Categoria();
		
		categorias_xid = categoriaServiceImpl.categoriaXID(id);
		
		System.out.println("Categoria XID: "+categorias_xid);
		
		return categorias_xid;
	}
	
	@PutMapping("/categoria/{id}")
	public Categoria actualizarCategoria(@PathVariable(name="id")Long id,@RequestBody Categoria categoria) {
		
		Categoria categoria_seleccionado= new Categoria();
		Categoria categoria_actualizado= new Categoria();
		
		categoria_seleccionado= categoriaServiceImpl.categoriaXID(id);
		
		categoria_seleccionado.setNombre(categoria.getNombre());
		categoria_seleccionado.setDescripcion(categoria.getDescripcion());
		
		categoria_actualizado = categoriaServiceImpl.actualizarCategoria(categoria_seleccionado);
		
		System.out.println("El Categoria actualizado es: "+ categoria_actualizado);
		
		return categoria_actualizado;
	}
	
	@DeleteMapping("/categoria/{id}")
	public void eleiminarCategoria(@PathVariable(name="id")Long id) {
		categoriaServiceImpl.eliminarCategoria(id);
	}
	
}