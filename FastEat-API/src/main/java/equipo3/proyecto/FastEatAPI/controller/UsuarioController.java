package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Usuario;
import equipo3.proyecto.FastEatAPI.service.UsuarioServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class UsuarioController{
	
	@Autowired
	private UsuarioServiceImpl usuarioServiceImpl;
	
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	public UsuarioController(UsuarioServiceImpl usuarioServiceImpl, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.usuarioServiceImpl = usuarioServiceImpl;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@GetMapping("/usuarios")
	public List<Usuario> listarUsuario(){
		return usuarioServiceImpl.listarUsuario();
	}
	
	@PostMapping("/usuario")
	public Usuario salvarUsuario(@RequestBody Usuario usuario) {
		usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
		return usuarioServiceImpl.guardarUsuario(usuario);
	}
	
	@GetMapping("/usuario/{id}")
	public Usuario usuariosXID(@PathVariable(name="id") Long id) {
		
		Usuario usuarios_xid= new Usuario();
		
		usuarios_xid = usuarioServiceImpl.usuarioXID(id);
		
		System.out.println("Usuario XID: "+usuarios_xid);
		
		return usuarios_xid;
	}
	
	@GetMapping("/usuario/username/{username}")
	public Usuario usuariosXUsername(@PathVariable(name="username") String username) {
		
		Usuario usuario= new Usuario();
		
		usuario = usuarioServiceImpl.loadUserByUsername(username);

		System.out.println("Usuario: "+usuario);
		
		return usuario;
	}
	
	@PutMapping("/usuario/updatePassword/{id}")
	public Usuario actualizarUsuario(@PathVariable(name="id")Long id,@RequestBody Usuario[] usuarios) {
		
		
		return null;
	}
	
	@PutMapping("/usuario/updateData/{id}")
	public Usuario actualizarUsuarioData(@PathVariable(name="id")Long id,@RequestBody Usuario usuario) {
		
		Usuario usuario_seleccionado= new Usuario();
		Usuario usuario_actualizado= new Usuario();
		
		usuario_seleccionado= usuarioServiceImpl.usuarioXID(id);
		
		usuario_seleccionado.setUsername(usuario.getUsername());
		usuario_seleccionado.setEmail(usuario.getEmail());
		usuario_seleccionado.setNombre(usuario.getNombre());
		usuario_seleccionado.setApellidos(usuario.getApellidos());
		
		usuario_actualizado = usuarioServiceImpl.actualizarUsuario(usuario_seleccionado);
		
		System.out.println("El Usuario actualizado es: "+ usuario_actualizado);
		
		return usuario_actualizado;
	}
	
	@DeleteMapping("/usuario/{id}")
	public void eleiminarUsuario(@PathVariable(name="id")Long id) {
		usuarioServiceImpl.eliminarUsuario(id);
	}
	
}
