package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Rol;
import equipo3.proyecto.FastEatAPI.service.RolServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class RolController {
	
	@Autowired
	RolServiceImpl rolServiceImpl;
	
	@GetMapping("/roles")
	public List<Rol> listarRol(){
		return rolServiceImpl.listarRol();
	}
	
	@PostMapping("/rol")
	public Rol salvarRol(@RequestBody Rol rol) {
		return rolServiceImpl.guardarRol(rol);
	}
		
	@GetMapping("/rol/{id}")
	public Rol rolsXID(@PathVariable(name="id") Long id) {
		
		Rol rols_xid= new Rol();
		
		rols_xid = rolServiceImpl.rolXID(id);
		
		System.out.println("Rol XID: "+rols_xid);
		
		return rols_xid;
	}
	
	@PutMapping("/rol/{id}")
	public Rol actualizarRol(@PathVariable(name="id")Long id,@RequestBody Rol rol) {
		
		Rol rol_seleccionado= new Rol();
		Rol rol_actualizado= new Rol();
		
		rol_seleccionado= rolServiceImpl.rolXID(id);
		
		rol_seleccionado.setNombre(rol.getNombre());
		
		rol_actualizado = rolServiceImpl.actualizarRol(rol_seleccionado);
		
		System.out.println("El Rol actualizado es: "+ rol_actualizado);
		
		return rol_actualizado;
	}
	
	@DeleteMapping("/rol/{id}")
	public void eleiminarRol(@PathVariable(name="id")Long id) {
		rolServiceImpl.eliminarRol(id);
	}
	
}