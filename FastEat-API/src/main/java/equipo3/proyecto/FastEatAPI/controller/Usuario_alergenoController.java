package equipo3.proyecto.FastEatAPI.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.FranjaHoraria;
import equipo3.proyecto.FastEatAPI.dto.Usuario_alergeno;
import equipo3.proyecto.FastEatAPI.service.Usuario_alergenoServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class Usuario_alergenoController{
	
	@Autowired
	Usuario_alergenoServiceImpl usuario_alergenoServiceImpl;
	
	@GetMapping("/usuarios_alergenos")
	public List<Usuario_alergeno> listarUsuario_alergeno(){
		return usuario_alergenoServiceImpl.listarUsuario_alergeno();
	}
	
	@PostMapping("/usuario_alergeno")
	public Usuario_alergeno salvarUsuario_alergeno(@RequestBody Usuario_alergeno usuario_alergeno) {
		return usuario_alergenoServiceImpl.guardarUsuario_alergeno(usuario_alergeno);
	}
		
	@GetMapping("/usuario_alergeno/{id}")
	public Usuario_alergeno usuario_alergenosXID(@PathVariable(name="id") Long id) {
		
		Usuario_alergeno usuario_alergenos_xid= new Usuario_alergeno();
		
		usuario_alergenos_xid = usuario_alergenoServiceImpl.usuario_alergenoXID(id);
		
		System.out.println("Usuario_alergeno XID: "+usuario_alergenos_xid);
		
		return usuario_alergenos_xid;
	}
	
	@GetMapping("/usuarios_alergenos/usuario/{id}")
	public List<Usuario_alergeno> AlergenosPorUsuario(@PathVariable(name="id") Long id) {
		
		// Lista que devolveremos
		List<Usuario_alergeno> lista = new ArrayList<Usuario_alergeno>();
		
		ArrayList alergenosPorUsuario = (ArrayList) usuario_alergenoServiceImpl.listarUsuario_alergeno();
		
	    Iterator<Usuario_alergeno> iteratorAlergenos = alergenosPorUsuario.iterator();
	    
	    int control = 0;
	    
	    while(iteratorAlergenos.hasNext()){
	    	Usuario_alergeno usuarioAlergeno = iteratorAlergenos.next();
	    	if (usuarioAlergeno.getUsuario().getId_usuario() == id) {
	    		lista.add(usuarioAlergeno);

	    		control ++;
	    	}
	        
	    }
	    if (control == 0) {
	    	return null;	
	    } else {
	    	return lista;
	    }
	}
	
	@PutMapping("/usuario_alergeno/{id}")
	public Usuario_alergeno actualizarUsuario_alergeno(@PathVariable(name="id")Long id,@RequestBody Usuario_alergeno usuario_alergeno) {
		
		Usuario_alergeno usuario_alergeno_seleccionado= new Usuario_alergeno();
		Usuario_alergeno usuario_alergeno_actualizado= new Usuario_alergeno();
		
		usuario_alergeno_seleccionado= usuario_alergenoServiceImpl.usuario_alergenoXID(id);
		
		usuario_alergeno_seleccionado.setAlergeno(usuario_alergeno.getAlergeno());
		usuario_alergeno_seleccionado.setUsuario(usuario_alergeno.getUsuario());
		
		usuario_alergeno_actualizado = usuario_alergenoServiceImpl.actualizarUsuario_alergeno(usuario_alergeno_seleccionado);
		
		System.out.println("El Usuario_alergeno actualizado es: "+ usuario_alergeno_actualizado);
		
		return usuario_alergeno_actualizado;
	}
	
	@DeleteMapping("/usuario_alergeno/{id}")
	public void eleiminarUsuario_alergeno(@PathVariable(name="id")Long id) {
		usuario_alergenoServiceImpl.eliminarUsuario_alergeno(id);
	}
	
	@DeleteMapping("/usuario_alergenos/usuario/{id}")
	public void EliminarAlergenosPorUsuario(@PathVariable(name="id") Long id) {
		
		ArrayList alergenosPorUsuario = (ArrayList) usuario_alergenoServiceImpl.listarUsuario_alergeno();
	    Iterator<Usuario_alergeno> iteratorAlergenos = alergenosPorUsuario.iterator();

	    while(iteratorAlergenos.hasNext()){
	    	Usuario_alergeno usuarioAlergeno = iteratorAlergenos.next();
	    	if (usuarioAlergeno.getUsuario().getId_usuario() == id) {
	    		usuario_alergenoServiceImpl.eliminarUsuario_alergeno(usuarioAlergeno.getId());
	    	}	      
	    }
	}
	
	
}