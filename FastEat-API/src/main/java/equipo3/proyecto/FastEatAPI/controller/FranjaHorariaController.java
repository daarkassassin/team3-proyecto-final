package equipo3.proyecto.FastEatAPI.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.FranjaHoraria;
import equipo3.proyecto.FastEatAPI.service.FranjaHorariaServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class FranjaHorariaController{
	
	@Autowired
	FranjaHorariaServiceImpl franjaHorariaServiceImpl;
	
	@GetMapping("/franjasHorarias")
	public List<FranjaHoraria> listarFranjaHoraria(){
		return franjaHorariaServiceImpl.listarFranjaHoraria();
	}
	
	@GetMapping("/franjasHorariasDisponibles")
	public List<FranjaHoraria> listarFranjaHorariaDisponible(){

		// Lista que devolveremos
		List<FranjaHoraria> lista = new ArrayList<FranjaHoraria>();
		
		ArrayList franjasHorarias = (ArrayList) franjaHorariaServiceImpl.listarFranjaHoraria();
		
	    Iterator<FranjaHoraria> iteratorFranjaHoraria = franjasHorarias.iterator();
	    
	    int control = 0;
	    
	    while(iteratorFranjaHoraria.hasNext()){
	    	FranjaHoraria franjaHoraria = iteratorFranjaHoraria.next();
	    	if (franjaHoraria.getOcupacionMaxima() > franjaHoraria.getOcupacionActual()) {
	    		lista.add(franjaHoraria);
	    		System.out.println(franjaHoraria.getId_franjahoraria());
	    		control ++;
	    	}
	        
	    }
	    if (control == 0) {
	    	return null;	
	    } else {
	    	return lista;
	    }
		
	}
	
	@PostMapping("/franjaHoraria")
	public FranjaHoraria salvarFranjaHoraria(@RequestBody FranjaHoraria franjaHoraria) {
		return franjaHorariaServiceImpl.guardarFranjaHoraria(franjaHoraria);
	}
		
	@GetMapping("/franjaHoraria/{id}")
	public FranjaHoraria franjaHorariasXID(@PathVariable(name="id") Long id) {
		
		FranjaHoraria franjaHorarias_xid= new FranjaHoraria();
		
		franjaHorarias_xid = franjaHorariaServiceImpl.franjaHorariaXID(id);
		
		System.out.println("FranjaHoraria XID: "+franjaHorarias_xid);
		
		return franjaHorarias_xid;
	}
	
	@PutMapping("/franjaHoraria/{id}")
	public FranjaHoraria actualizarFranjaHoraria(@PathVariable(name="id")Long id,@RequestBody FranjaHoraria franjaHoraria) {
		
		FranjaHoraria franjaHoraria_seleccionado= new FranjaHoraria();
		FranjaHoraria franjaHoraria_actualizado= new FranjaHoraria();
		
		franjaHoraria_seleccionado= franjaHorariaServiceImpl.franjaHorariaXID(id);
		
		franjaHoraria_seleccionado.setHoraInicio(franjaHoraria.getHoraInicio());
		franjaHoraria_seleccionado.setHoraFinal(franjaHoraria.getHoraFinal());
		franjaHoraria_seleccionado.setOcupacionMaxima(franjaHoraria.getOcupacionMaxima());
		franjaHoraria_seleccionado.setOcupacionActual(franjaHoraria.getOcupacionActual());
		
		
		franjaHoraria_actualizado = franjaHorariaServiceImpl.actualizarFranjaHoraria(franjaHoraria_seleccionado);
		
		System.out.println("El FranjaHoraria actualizado es: "+ franjaHoraria_actualizado);
		
		return franjaHoraria_actualizado;
	}
	
	@DeleteMapping("/franjaHoraria/{id}")
	public void eleiminarFranjaHoraria(@PathVariable(name="id")Long id) {
		franjaHorariaServiceImpl.eliminarFranjaHoraria(id);
	}
	
}