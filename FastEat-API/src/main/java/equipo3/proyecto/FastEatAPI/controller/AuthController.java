package equipo3.proyecto.FastEatAPI.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Usuario;
import equipo3.proyecto.FastEatAPI.service.UsuarioServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("")
public class AuthController {
	@Autowired
	UsuarioServiceImpl usuarioServiceImpl;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@RequestBody Usuario usuario) {
		
		//Comprobamos si el username ya existe en la BD
		if (usuarioServiceImpl.existsByUsername(usuario.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body("Error: Este nombre de usuario ya existe!");
		}

		//Comprobamos si el email ya existe en la BD
		if (usuarioServiceImpl.existsByEmail(usuario.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body("Error: El correo electrónico ya está en uso!");
		}

		//Creamos un nuevo usuario
		usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
		usuarioServiceImpl.guardarUsuario(usuario);
		
		return ResponseEntity.ok("\"El usuario se registro correctamente!\"");
	}
}

