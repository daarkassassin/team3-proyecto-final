package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Rol_usuario;
import equipo3.proyecto.FastEatAPI.service.Rol_usuarioServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class Rol_usuarioController{
	
	@Autowired
	Rol_usuarioServiceImpl rol_usuarioServiceImpl;
	
	@GetMapping("/roles_usuarios")
	public List<Rol_usuario> listarRol_usuario(){
		return rol_usuarioServiceImpl.listarRol_usuario();
	}
	
	@PostMapping("/rol_usuario")
	public Rol_usuario salvarRol_usuario(@RequestBody Rol_usuario rol_usuario) {
		return rol_usuarioServiceImpl.guardarRol_usuario(rol_usuario);
	}
		
	@GetMapping("/rol_usuario/{id}")
	public Rol_usuario rol_usuarioXID(@PathVariable(name="id") Long id) {
		
		Rol_usuario rol_usuario_xid= new Rol_usuario();
		
		rol_usuario_xid = rol_usuarioServiceImpl.rol_usuarioXID(id);
		
		System.out.println("Rol_usuario XID: "+rol_usuario_xid);
		
		return rol_usuario_xid;
	}
	
	@PutMapping("/rol_usuario/{id}")
	public Rol_usuario actualizarRol_usuario(@PathVariable(name="id")Long id,@RequestBody Rol_usuario rol_usuario) {
		
		Rol_usuario rol_usuario_seleccionado= new Rol_usuario();
		Rol_usuario rol_usuario_actualizado= new Rol_usuario();
		
		rol_usuario_seleccionado= rol_usuarioServiceImpl.rol_usuarioXID(id);
		
		rol_usuario_seleccionado.setRol(rol_usuario.getRol());
		rol_usuario_seleccionado.setUsuario(rol_usuario.getUsuario());
		
		rol_usuario_actualizado = rol_usuarioServiceImpl.actualizarRol_usuario(rol_usuario_seleccionado);
		
		System.out.println("El Rol_usuario actualizado es: "+ rol_usuario_actualizado);
		
		return rol_usuario_actualizado;
	}
	
	@DeleteMapping("/rol_usuario/{id}")
	public void eleiminarRol_usuario(@PathVariable(name="id")Long id) {
		rol_usuarioServiceImpl.eliminarRol_usuario(id);
	}
	
}
