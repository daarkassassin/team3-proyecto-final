package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Pedido_Item;
import equipo3.proyecto.FastEatAPI.service.Pedido_ItemServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class Pedido_ItemController{
	
	@Autowired
	Pedido_ItemServiceImpl pedido_ItemServiceImpl;
	
	@GetMapping("/pedidos_Items")
	public List<Pedido_Item> listarPedido_Item(){
		return pedido_ItemServiceImpl.listarPedido_Item();
	}
	
	@PostMapping("/pedido_Item")
	public Pedido_Item salvarPedido_Item(@RequestBody Pedido_Item pedido_Item) {
		return pedido_ItemServiceImpl.guardarPedido_Item(pedido_Item);
	}
		
	@GetMapping("/pedido_Item/{id}")
	public Pedido_Item pedido_ItemsXID(@PathVariable(name="id") Long id) {
		
		Pedido_Item pedido_Items_xid= new Pedido_Item();
		
		pedido_Items_xid = pedido_ItemServiceImpl.pedido_ItemXID(id);
		
		System.out.println("Pedido_Item XID: "+pedido_Items_xid);
		
		return pedido_Items_xid;
	}
	
	@PutMapping("/pedido_Item/{id}")
	public Pedido_Item actualizarPedido_Item(@PathVariable(name="id")Long id,@RequestBody Pedido_Item pedido_Item) {
		
		Pedido_Item pedido_Item_seleccionado= new Pedido_Item();
		Pedido_Item pedido_Item_actualizado= new Pedido_Item();
		
		pedido_Item_seleccionado= pedido_ItemServiceImpl.pedido_ItemXID(id);
		
		pedido_Item_seleccionado.setPedido(pedido_Item.getPedido());
		pedido_Item_seleccionado.setItem(pedido_Item.getItem());
		
		pedido_Item_actualizado = pedido_ItemServiceImpl.actualizarPedido_Item(pedido_Item_seleccionado);
		
		System.out.println("El Pedido_Item actualizado es: "+ pedido_Item_actualizado);
		
		return pedido_Item_actualizado;
	}
	
	@DeleteMapping("/pedido_Item/{id}")
	public void eleiminarPedido_Item(@PathVariable(name="id")Long id) {
		pedido_ItemServiceImpl.eliminarPedido_Item(id);
	}
	
}