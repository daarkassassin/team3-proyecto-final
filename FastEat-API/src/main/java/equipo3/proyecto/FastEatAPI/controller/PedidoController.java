package equipo3.proyecto.FastEatAPI.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import equipo3.proyecto.FastEatAPI.dto.Pedido;
import equipo3.proyecto.FastEatAPI.service.PedidoServiceImpl;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api")
public class PedidoController{
	
	@Autowired
	PedidoServiceImpl pedidoServiceImpl;
	
	@GetMapping("/pedidos")
	public List<Pedido> listarPedido(){
		return pedidoServiceImpl.listarPedido();
	}
	
	@PostMapping("/pedido")
	public Pedido salvarPedido(@RequestBody Pedido pedido) {
		return pedidoServiceImpl.guardarPedido(pedido);
	}
		
	@GetMapping("/pedido/{id}")
	public Pedido pedidosXID(@PathVariable(name="id") Long id) {
		
		Pedido pedidos_xid= new Pedido();
		
		pedidos_xid = pedidoServiceImpl.pedidoXID(id);
		
		System.out.println("Pedido XID: "+pedidos_xid);
		
		return pedidos_xid;
	}
	
	@PutMapping("/pedido/{id}")
	public Pedido actualizarPedido(@PathVariable(name="id")Long id,@RequestBody Pedido pedido) {
		
		Pedido pedido_seleccionado= new Pedido();
		Pedido pedido_actualizado= new Pedido();
		
		pedido_seleccionado= pedidoServiceImpl.pedidoXID(id);
		
		pedido_seleccionado.setFecha(pedido.getFecha());
		pedido_seleccionado.setPrecioTotal(pedido.getPrecioTotal());
		pedido_seleccionado.setIva(pedido.getIva());
		pedido_seleccionado.setUsuario(pedido.getUsuario());
		pedido_seleccionado.setFranjaHoraria(pedido.getFranjaHoraria());
		
		pedido_actualizado = pedidoServiceImpl.actualizarPedido(pedido_seleccionado);
		
		System.out.println("El Pedido actualizado es: "+ pedido_actualizado);
		
		return pedido_actualizado;
	}
	
	@DeleteMapping("/pedido/{id}")
	public void eleiminarPedido(@PathVariable(name="id")Long id) {
		pedidoServiceImpl.eliminarPedido(id);
	}
	
}