package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IUsuario_alergenoDAO;
import equipo3.proyecto.FastEatAPI.dto.Usuario_alergeno;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IUsuario_alergenoService;

@Service
public class Usuario_alergenoServiceImpl implements IUsuario_alergenoService{
	
	@Autowired
	IUsuario_alergenoDAO iUsuario_alergenoDAO;

	@Override
	public List<Usuario_alergeno> listarUsuario_alergeno() {
		return iUsuario_alergenoDAO.findAll();
	}

	@Override
	public Usuario_alergeno guardarUsuario_alergeno(Usuario_alergeno usuario_alergeno) {
		return iUsuario_alergenoDAO.save(usuario_alergeno);
	}

	@Override
	public Usuario_alergeno usuario_alergenoXID(Long id) {
		return iUsuario_alergenoDAO.findById(id).get();
	}

	@Override
	public Usuario_alergeno actualizarUsuario_alergeno(Usuario_alergeno usuario_alergeno) {
		return iUsuario_alergenoDAO.save(usuario_alergeno);
	}

	@Override
	public void eliminarUsuario_alergeno(Long id) {
		iUsuario_alergenoDAO.deleteById(id);
	}

}
