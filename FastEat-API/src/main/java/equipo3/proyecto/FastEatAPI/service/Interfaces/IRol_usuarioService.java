package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Rol_usuario;

public interface IRol_usuarioService {
	
	public List<Rol_usuario> listarRol_usuario();
	
	public Rol_usuario guardarRol_usuario(Rol_usuario rol_usuario);
	
	public Rol_usuario rol_usuarioXID(Long id);
	
	public Rol_usuario actualizarRol_usuario(Rol_usuario rol_usuario);
	
	public void eliminarRol_usuario(Long id);

}
