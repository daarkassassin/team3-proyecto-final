package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IItem_AlergenoDAO;
import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.dto.Item_Alergeno;
import equipo3.proyecto.FastEatAPI.dto.Usuario_alergeno;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IItem_AlergenoService;

@Service
public class Item_AlergenoServiceImpl implements IItem_AlergenoService{
	
	@Autowired
	IItem_AlergenoDAO iItem_AlergenoDAO;

	@Override
	public List<Item_Alergeno> listarItem_Alergeno() {
		return iItem_AlergenoDAO.findAll();
	}

	@Override
	public Item_Alergeno guardarItem_Alergeno(Item_Alergeno item_Alergeno) {
		return iItem_AlergenoDAO.save(item_Alergeno);
	}

	@Override
	public Item_Alergeno item_AlergenoXID(Long id) {
		return iItem_AlergenoDAO.findById(id).get();
	}

	@Override
	public Item_Alergeno actualizarItem_Alergeno(Item_Alergeno item_Alergeno) {
		return iItem_AlergenoDAO.save(item_Alergeno);
	}

	@Override
	public void eliminarItem_Alergeno(Long id) {
		iItem_AlergenoDAO.deleteById(id);
	}

	@Override
	public List<Item_Alergeno> item_AlergenoXItem(Item item) {
		return iItem_AlergenoDAO.findByItem(item);
	}

	@Override
	public void eliminarbyItem(Item item) {
		iItem_AlergenoDAO.deleteByItem(item);
		
	}

}
