package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.FranjaHoraria;

public interface IFranjaHorariaService {
	
	public List<FranjaHoraria> listarFranjaHoraria();
	
	public FranjaHoraria guardarFranjaHoraria(FranjaHoraria franjaHoraria);
	
	public FranjaHoraria franjaHorariaXID(Long id);
	
	public FranjaHoraria actualizarFranjaHoraria(FranjaHoraria franjaHoraria);
	
	public void eliminarFranjaHoraria(Long id);

}
