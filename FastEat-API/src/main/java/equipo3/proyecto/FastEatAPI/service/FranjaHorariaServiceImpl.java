package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IFranjaHorariaDAO;
import equipo3.proyecto.FastEatAPI.dto.FranjaHoraria;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IFranjaHorariaService;

@Service
public class FranjaHorariaServiceImpl implements IFranjaHorariaService{
	
	@Autowired
	IFranjaHorariaDAO iFranjaHorariaDAO;

	@Override
	public List<FranjaHoraria> listarFranjaHoraria() {
		return iFranjaHorariaDAO.findAll();
	}

	@Override
	public FranjaHoraria guardarFranjaHoraria(FranjaHoraria franjaHoraria) {
		return iFranjaHorariaDAO.save(franjaHoraria);
	}

	@Override
	public FranjaHoraria franjaHorariaXID(Long id) {
		return iFranjaHorariaDAO.findById(id).get();
	}

	@Override
	public FranjaHoraria actualizarFranjaHoraria(FranjaHoraria franjaHoraria) {
		return iFranjaHorariaDAO.save(franjaHoraria);
	}

	@Override
	public void eliminarFranjaHoraria(Long id) {
		iFranjaHorariaDAO.deleteById(id);
	}

}
