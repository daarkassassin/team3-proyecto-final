package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.ICategoriaDAO;
import equipo3.proyecto.FastEatAPI.dto.Categoria;
import equipo3.proyecto.FastEatAPI.service.Interfaces.ICategoriaService;

@Service
public class CategoriaServiceImpl implements ICategoriaService{
	
	@Autowired
	ICategoriaDAO iCategoriaDAO;

	@Override
	public List<Categoria> listarCategoria() {
		return iCategoriaDAO.findAll();
	}

	@Override
	public Categoria guardarCategoria(Categoria categoria) {
		return iCategoriaDAO.save(categoria);
	}

	@Override
	public Categoria categoriaXID(Long id) {
		return iCategoriaDAO.findById(id).get();
	}

	@Override
	public Categoria actualizarCategoria(Categoria categoria) {
		return iCategoriaDAO.save(categoria);
	}

	@Override
	public void eliminarCategoria(Long id) {
		iCategoriaDAO.deleteById(id);
	}

}
