package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Rol;

public interface IRolService {
	
	public List<Rol> listarRol();
	
	public Rol guardarRol(Rol rol);
	
	public Rol rolXID(Long id);
	
	public Rol actualizarRol(Rol rol);
	
	public void eliminarRol(Long id);

}
