package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IItemDAO;
import equipo3.proyecto.FastEatAPI.dto.Categoria;
import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IItemService;

@Service
public class ItemServiceImpl implements IItemService{
	
	@Autowired
	IItemDAO iItemDAO;

	@Override
	public List<Item> listarItem() {
		return iItemDAO.findAll();
	}
	
	@Override
	public Item guardarItem(Item item) {
		return iItemDAO.save(item);
	}

	@Override
	public Item itemXID(Long id) {
		return iItemDAO.findById(id).get();
	}

	@Override
	public Item actualizarItem(Item item) {
		return iItemDAO.save(item);
	}

	@Override
	public void eliminarItem(Long id) {
		iItemDAO.deleteById(id);
	}

}
