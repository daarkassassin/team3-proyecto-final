package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IUsuarioDAO;
import equipo3.proyecto.FastEatAPI.dto.Usuario;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService{
	
	@Autowired
	IUsuarioDAO iUsuarioDAO;

	@Override
	public List<Usuario> listarUsuario() {
		return iUsuarioDAO.findAll();
	}

	@Override
	public Usuario guardarUsuario(Usuario usuario) {
		return iUsuarioDAO.save(usuario);
	}

	@Override
	public Usuario usuarioXID(Long id) {
		return iUsuarioDAO.findById(id).get();
	}

	@Override
	public Usuario actualizarUsuario(Usuario usuario) {
		return iUsuarioDAO.save(usuario);
	}

	@Override
	public void eliminarUsuario(Long id) {
		iUsuarioDAO.deleteById(id);
	}
	
	@Override
	public Boolean existsByUsername(String username) {
		return iUsuarioDAO.existsByUsername(username);
	}
	
	@Override
	public Boolean existsByEmail(String email) {
		return iUsuarioDAO.existsByEmail(email);
	}

	@Override
	public Usuario loadUserByUsername(String username) {
		return  iUsuarioDAO.findByUsername(username);
	}

}
