package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IAlergenoDAO;
import equipo3.proyecto.FastEatAPI.dto.Alergeno;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IAlergenoService;

@Service
public class AlergenoServiceImpl implements IAlergenoService{
	
	@Autowired
	IAlergenoDAO iAlergenoDAO;

	@Override
	public List<Alergeno> listarAlergeno() {
		return iAlergenoDAO.findAll();
	}

	@Override
	public Alergeno guardarAlergeno(Alergeno alergeno) {
		return iAlergenoDAO.save(alergeno);
	}

	@Override
	public Alergeno alergenoXID(Long id) {
		return iAlergenoDAO.findById(id).get();
	}

	@Override
	public Alergeno actualizarAlergeno(Alergeno alergeno) {
		return iAlergenoDAO.save(alergeno);
	}

	@Override
	public void eliminarAlergeno(Long id) {
		iAlergenoDAO.deleteById(id);
	}

}
