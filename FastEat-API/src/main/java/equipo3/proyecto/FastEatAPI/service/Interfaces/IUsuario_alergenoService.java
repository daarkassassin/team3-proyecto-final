package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.dto.Usuario_alergeno;

public interface IUsuario_alergenoService {
	
	public List<Usuario_alergeno> listarUsuario_alergeno();
	
	public Usuario_alergeno guardarUsuario_alergeno(Usuario_alergeno usuario_alergeno);
	
	public Usuario_alergeno usuario_alergenoXID(Long id);
	
	public Usuario_alergeno actualizarUsuario_alergeno(Usuario_alergeno usuario_alergeno);
	
	public void eliminarUsuario_alergeno(Long id);
	
	
	
	

}
