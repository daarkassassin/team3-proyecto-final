package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Categoria;

public interface ICategoriaService {
	
	public List<Categoria> listarCategoria();
	
	public Categoria guardarCategoria(Categoria categoria);
	
	public Categoria categoriaXID(Long id);
	
	public Categoria actualizarCategoria(Categoria categoria);
	
	public void eliminarCategoria(Long id);

}
