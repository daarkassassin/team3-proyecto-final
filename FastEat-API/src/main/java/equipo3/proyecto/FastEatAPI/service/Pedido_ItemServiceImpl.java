package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IPedido_ItemDAO;
import equipo3.proyecto.FastEatAPI.dto.Pedido_Item;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IPedido_ItemService;

@Service
public class Pedido_ItemServiceImpl implements IPedido_ItemService{
	
	@Autowired
	IPedido_ItemDAO iPedido_ItemDAO;

	@Override
	public List<Pedido_Item> listarPedido_Item() {
		return iPedido_ItemDAO.findAll();
	}

	@Override
	public Pedido_Item guardarPedido_Item(Pedido_Item pedido_Item) {
		return iPedido_ItemDAO.save(pedido_Item);
	}

	@Override
	public Pedido_Item pedido_ItemXID(Long id) {
		return iPedido_ItemDAO.findById(id).get();
	}

	@Override
	public Pedido_Item actualizarPedido_Item(Pedido_Item pedido_Item) {
		return iPedido_ItemDAO.save(pedido_Item);
	}

	@Override
	public void eliminarPedido_Item(Long id) {
		iPedido_ItemDAO.deleteById(id);
	}

}
