package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IRolDAO;
import equipo3.proyecto.FastEatAPI.dto.Rol;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IRolService;

@Service
public class RolServiceImpl implements IRolService{
	
	@Autowired
	IRolDAO iRolDAO;

	@Override
	public List<Rol> listarRol() {
		return iRolDAO.findAll();
	}

	@Override
	public Rol guardarRol(Rol rol) {
		return iRolDAO.save(rol);
	}

	@Override
	public Rol rolXID(Long id) {
		return iRolDAO.findById(id).get();
	}

	@Override
	public Rol actualizarRol(Rol rol) {
		return iRolDAO.save(rol);
	}

	@Override
	public void eliminarRol(Long id) {
		iRolDAO.deleteById(id);
	}

}