package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Alergeno;

public interface IAlergenoService {
	
	public List<Alergeno> listarAlergeno();
	
	public Alergeno guardarAlergeno(Alergeno alergeno);
	
	public Alergeno alergenoXID(Long id);
	
	public Alergeno actualizarAlergeno(Alergeno alergeno);
	
	public void eliminarAlergeno(Long id);

}
