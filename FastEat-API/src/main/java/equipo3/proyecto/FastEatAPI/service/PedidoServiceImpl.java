package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IPedidoDAO;
import equipo3.proyecto.FastEatAPI.dto.Pedido;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IPedidoService;

@Service
public class PedidoServiceImpl implements IPedidoService{
	
	@Autowired
	IPedidoDAO iPedidoDAO;

	@Override
	public List<Pedido> listarPedido() {
		return iPedidoDAO.findAll();
	}

	@Override
	public Pedido guardarPedido(Pedido pedido) {
		return iPedidoDAO.save(pedido);
	}

	@Override
	public Pedido pedidoXID(Long id) {
		return iPedidoDAO.findById(id).get();
	}

	@Override
	public Pedido actualizarPedido(Pedido pedido) {
		return iPedidoDAO.save(pedido);
	}

	@Override
	public void eliminarPedido(Long id) {
		iPedidoDAO.deleteById(id);
	}

}
