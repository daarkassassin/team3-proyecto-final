package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Pedido_Item;

public interface IPedido_ItemService {
	
	public List<Pedido_Item> listarPedido_Item();
	
	public Pedido_Item guardarPedido_Item(Pedido_Item pedido_Item);
	
	public Pedido_Item pedido_ItemXID(Long id);
	
	public Pedido_Item actualizarPedido_Item(Pedido_Item pedido_Item);
	
	public void eliminarPedido_Item(Long id);

}
