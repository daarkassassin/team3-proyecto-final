package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Item;
import equipo3.proyecto.FastEatAPI.dto.Item_Alergeno;
public interface IItem_AlergenoService {
	
	public List<Item_Alergeno> listarItem_Alergeno();
	
	public Item_Alergeno guardarItem_Alergeno(Item_Alergeno item_Alergeno);
	
	public Item_Alergeno item_AlergenoXID(Long id);
	
	public Item_Alergeno actualizarItem_Alergeno(Item_Alergeno item_Alergeno);
	
	public void eliminarItem_Alergeno(Long id);
	
	public List<Item_Alergeno> item_AlergenoXItem(Item item);
	
	public void eliminarbyItem(Item item);

}
