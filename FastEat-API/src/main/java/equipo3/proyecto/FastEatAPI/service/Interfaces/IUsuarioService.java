package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Usuario;

public interface IUsuarioService {
	
	public List<Usuario> listarUsuario();
	
	public Usuario guardarUsuario(Usuario usuario);
	
	public Usuario usuarioXID(Long id);
	
	public Usuario actualizarUsuario(Usuario usuario);
	
	public void eliminarUsuario(Long id);
	
	public Boolean existsByUsername(String username);
	
	public Boolean existsByEmail(String email);
	
	public Usuario loadUserByUsername(String username);

}
