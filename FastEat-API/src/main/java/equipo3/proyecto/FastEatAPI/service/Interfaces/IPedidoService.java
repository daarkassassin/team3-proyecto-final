package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Pedido;

public interface IPedidoService {
	
	public List<Pedido> listarPedido();
	
	public Pedido guardarPedido(Pedido pedido);
	
	public Pedido pedidoXID(Long id);
	
	public Pedido actualizarPedido(Pedido pedido);
	
	public void eliminarPedido(Long id);

}
