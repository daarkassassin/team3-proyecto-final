package equipo3.proyecto.FastEatAPI.service.Interfaces;

import java.util.List;

import equipo3.proyecto.FastEatAPI.dto.Item;

public interface IItemService {
	
	public List<Item> listarItem();
	
	public Item guardarItem(Item item);
	
	public Item itemXID(Long id);
	
	public Item actualizarItem(Item item);
	
	public void eliminarItem(Long id);

}
