package equipo3.proyecto.FastEatAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import equipo3.proyecto.FastEatAPI.dao.IRol_usuarioDAO;
import equipo3.proyecto.FastEatAPI.dto.Rol_usuario;
import equipo3.proyecto.FastEatAPI.service.Interfaces.IRol_usuarioService;

@Service
public class Rol_usuarioServiceImpl implements IRol_usuarioService{
	
	@Autowired
	IRol_usuarioDAO iRol_usuarioDAO;

	@Override
	public List<Rol_usuario> listarRol_usuario() {
		return iRol_usuarioDAO.findAll();
	}

	@Override
	public Rol_usuario guardarRol_usuario(Rol_usuario rol_usuario) {
		return iRol_usuarioDAO.save(rol_usuario);
	}

	@Override
	public Rol_usuario rol_usuarioXID(Long id) {
		return iRol_usuarioDAO.findById(id).get();
	}

	@Override
	public Rol_usuario actualizarRol_usuario(Rol_usuario rol_usuario) {
		return iRol_usuarioDAO.save(rol_usuario);
	}

	@Override
	public void eliminarRol_usuario(Long id) {
		iRol_usuarioDAO.deleteById(id);
	}

}
